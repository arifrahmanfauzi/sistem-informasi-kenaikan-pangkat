<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Cara Install

Download ke local komputer kalian bisa pakai
```sh
$ git clone https://gitlab.com/arifrahman.fauzi/sistem-informasi-kenaikan-pangkat.git
```

- Buat Databasenya namanya terserah defaultnya nama databasenya [siskp].
- eksport sqlnya ke database kalian.
- masuk ke folder sistem-informasi-kenaikan-pangkat.
- rename file `.env.example` ke `.env`.
- ketikan perintah ini :
```sh
$ composer install
$ php artisan:key generate
```



- `kalau file .env tidak ada kalian bisa copy dari project laravel lain kemudian di sesuaikan konfigurasinya sesuai punya kalian atau bisa download di sini ->`
[.env.example](https://github.com/laravel/laravel/blob/master/.env.example)



- untuk login ada 3 jenis : `arif, fakultas, kepegawaian ` untuk Password `[123456]`.

- `Gambar Relasi Databasenya`

<img src="siskp.PNG">






## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
