<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();
Route::get('/users','HomeController@users');
//Route::get('/pak', 'HomeController@getunsur');
Route::get('/pak/{id}', 'HomeController@getunsur');
Route::get('getsubunsur','HomeController@getsubunsur' );
Route::get('getsubbidang','HomeController@getsubbidang' );
Route::get('getangkakredit', 'HomeController@getangkakredit');
Route::post('getpengajuan','Pengajuan@update' );
Route::post('postpak','AngkaKreditController@postpak');
Route::get('/profile','HomeController@profile')->name('profile'); //profile si dosen -> profile.blade.php
Route::get('/table', function()
{
  return view('table');
});
Route::get('/user', function()
{
  return view('user');
});
Route::get('/input/angkakredit', 'AngkaKreditController@index')->name('angkakredit');
//Route::get('/metu', 'HomeController@logout')->name('logout');
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware'=>'auth'], function () {

	Route::get('permissions-all-users',['middleware'=>'check-permission:user|admin|superadmin','uses'=>'HomeController@allUsers']);

	Route::get('permissions-admin-superadmin',['middleware'=>'check-permission:admin|superadmin','uses'=>'HomeController@adminSuperadmin']);

	Route::get('permissions-superadmin',['middleware'=>'check-permission:superadmin','uses'=>'HomeController@superadmin']);

});
Route::resource('pengajuan', 'Pengajuan');

//punya kepegawaian dan penilai
Route::resource('penilai-internal','PenilaiInternalController'); // menampilkan master penilai internal
Route::resource('riwayat','RiwayatController'); // menampilkan riwayat menilai dari penilaian
Route::resource('penilai-eksternal','PenilaiEksternalController'); // menampilkan master penilai eksternal
Route::get('/ubah-penilai/{id}','PenilaiEksternalController@edit_data' ); // menuju tampilan edit
Route::resource('pilih-penilai','PilihPenilaiController'); // memilih penilai
Route::resource('penilaian','NilaiPAKController'); // menampilkan data pengajuan yang akan dinilai
Route::get('/nilai-pak/{id}', 'NilaiPAKController@getunsur2'); // tampilan penilaian PAK
Route::get('/isi-nilai/{id}', 'NilaiPAKController@dapat_rubrik'); // dapatkan id rubrik
Route::get('tambah-penilai','PenilaiEksternalController@tambah_data'); // tampilan tambah data penilai eksternal
Route::delete('/hapus-penilai/{id}','PenilaiEksternalController@destroy'); // hapus data penilai eksternal
Route::get('dupak','HomeController@dupak');//ke view dupak

Route::post('insertpenilai/{id}','PilihPenilaiController@pilih');
Route::get('/data', 'fakultas@index');
Route::post('/data/update', 'fakultas@update');
Route::get('/suket/{id}', 'fakultas@suket');

// Route::get('/pengajuan', 'DosenController@pengajuan');
// Route::post('postpengajuan', 'DosenController@postpengajuan')->name('pengajuan');
