
@extends('layouts.template')
@section('css')
  <!-- start: Css -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/bootstrap.min.css')}}">

  <!-- plugins -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/datatables.bootstrap.min.css')}}"/>
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/font-awesome.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/animate.min.css')}}" />
  <link href="{{asset('asset/css/style.css')}}" rel="stylesheet">
  <!-- end: Css -->

  <!-- end: Css -->
  <!-- end: Css -->

  <link rel="shortcut icon" href="{{asset('asset/img/logomi.png')}}">

@endsection

@section('profile')
  <li class="user-name"><span>{{auth::user()->username}}</span></li>
                    <li class="dropdown avatar-dropdown">
                     <img src="{{asset('asset/img/avatar.jpg')}}" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
                     <ul class="dropdown-menu user-dropdown">
                       <li><a href="#"><span class="fa fa-user"></span> My Profile</a></li>
                       <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();"><span class="fa fa-power-off"></span> Logout</a></li>
                          {{-- Di panggil pada event onclick --}}
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>

                        </ul>
                      </li>
@endsection
@section('leftmenu')

@endsection
@section('content')
            <!-- edit penilai eksternal -->
            <div id="content">
              <div class="tab-wrapper text-center">
                <div class="panel box-shadow-none text-left content-header">
                  <div class="panel-body" style="padding-bottom:0px">
                    <div class="col-md-12">
                      <h3 class="animated fadeInLeft"> Data Penilai Eksternal</h3>
                      <p class="animated fadeInDown">
                          Penilai  <span class="fa-angle-right fa"></span>  Edit Penilai Eksternal
                              </p>
                    </div>

                  </div>
                  <div class="col-md-12 top-20 padding-0">
                    <div class="col-md-12">
                        <div class="panel">
                          <div class="panel-heading"><h3>Edit Penilai Eksternal </h3>
                       </div>
                    <div class="panel-body">
                                      @foreach ( $eksternal as $data)
                                      <form id="form" class="" action="{{ route('penilai-eksternal.update', $data->id_penilai_eks) }}" method="post">
                                        <input type="hidden" name="id" value="{{ $data->id_penilai_eks }}">
                                        {{ csrf_field() }}
                                        {{ method_field('PUT') }}
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-2 control-label text-right" style="padding-top: 6px; font-size: 15px;">Nama</label>
                                         <div class="col-sm-8"><input type="text" class="form-control nama" id="nama" name="nama" value="{{ $data->nama }}"></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-2 control-label text-right" style="padding-top: 6px; font-size: 15px;">NIP</label>
                                         <div class="col-sm-8"><input type="text" class="form-control nip" id="nip" name="nip" value="{{ $data->nip }}"></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-2 control-label text-right" style="padding-top: 6px; font-size: 15px;">NIDN</label>
                                         <div class="col-sm-8"><input type="text" class="form-control nidn" id="nidn" name="nidn" value="{{ $data->nidn }}"></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-2 control-label text-right" style="padding-top: 6px; font-size: 15px;">Password</label>
                                         <div class="col-sm-8"><input type="password" class="form-control password" id="password" name="password" value="{{ $data->password }}"></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-2 control-label text-right" style="padding-top: 6px; font-size: 15px;">Universitas</label>
                                          <div class="col-sm-8">
                                            <select class="form-control universitas" id="universitas" name="universitas">
                                              @foreach($universitas as $univ)
                                                @if ($univ->nama_univ == $data->universitas)
                                                <option {{ $univ->nama_univ == $data->universitas ? 'selected':'' }}> {{$data->universitas}} </option>
                                                @else
                                                <option value="{{ $univ->nama_univ }}">{{ $univ->nama_univ}}</option>
                                                @endif
                                              @endforeach
                                            </select>
                                          </div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px">
                                          <label class="col-sm-2 control-label text-right" style="padding-top: 6px; font-size: 15px;">Rumpun</label>
                                          <div class="col-sm-8">
                                            <select class="form-control rumpun" id="rumpun" name="rumpun">
                                              @foreach($rumpun as $rump)
                                                @if ($rump->nama_rumpun == $data->rumpun)
                                                <option {{ $rump->nama_rumpun == $data->rumpun ? 'selected':'' }}> {{$data->rumpun}} </option>
                                                @else
                                                <option value="{{ $rump->nama_rumpun }}">{{ $rump->nama_rumpun}}</option>
                                                @endif
                                              @endforeach
                                            </select>
                                          </div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-2 control-label text-right" style="padding-top: 6px; font-size: 15px;">Sub Rumpun</label>
                                         <div class="col-sm-8">
                                            <select class="form-control sub_rumpun" id="sub_rumpun" name="sub_rumpun">
                                              @foreach($sub_rumpun as $srump)
                                                @if ($srump->nama_rumpun_sub == $data->sub_rumpun)
                                                <option {{ $srump->nama_rumpun_sub == $data->sub_rumpun ? 'selected':'' }}> {{$data->sub_rumpun}} </option>
                                                @else
                                                <option value="{{ $srump->nama_rumpun_sub }}">{{ $srump->nama_rumpun_sub}}</option>
                                                @endif
                                              @endforeach
                                            </select>
                                          </div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-2 control-label text-right" style="padding-top: 6px; font-size: 15px;">Jabatan Fungsional</label>
                                          <div class="col-sm-8">
                                            <select class="form-control jabatan" id="jabatan" name="jabatan">
                                               @foreach($jabatan as $jab)
                                                  @if ($jab->nama_jafung == $data->jabatan)
                                                  <option {{ $jab->nama_jafung == $data->jabatan ? 'selected':'' }}> {{$data->jabatan}} </option>
                                                  @else
                                                  <option value="{{ $jab->nama_jafung }}">{{ $jab->nama_jafung }}</option>
                                                  @endif
                                               @endforeach
                                            </select>
                                          </div>
                                        </div>
                                        
                                          <button type="reset" class="btn btn-default" data-dismiss="modal">Batal</button>
                                          <button type="submit" class="btn btn-primary">Simpan</button>
                                        
                                    </form>
                                  @endforeach
                </div>
              </div>

              </div>
            </div>
          </div>
                        <!-- end: content -->
@endsection
@section('javascript')
<!-- start: Javascript -->
  <script src="{{ asset('asset/js/jquery.min.js') }}"></script>
  <script src="{{ asset('asset/js/jquery.ui.min.js') }}"></script>
  <script src="{{ asset('asset/js/bootstrap.min.js') }}"></script>
<!-- plugins -->
  <script src="{{ asset('asset/js/plugins/moment.min.js') }}"></script>
  <script src="{{ asset('asset/js/plugins/jquery.datatables.min.js') }}"></script>
  <script src="{{ asset('asset/js/plugins/datatables.bootstrap.min.js') }}"></script>
  <script src="{{ asset('asset/js/plugins/jquery.nicescroll.js') }}"></script>
<!-- custom -->
  <script src="{{ asset('asset/js/main.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#datatables-example').DataTable();
  });
</script>
<!-- end: Javascript -->
@endsection
