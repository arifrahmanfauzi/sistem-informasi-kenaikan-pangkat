@extends('layouts.template')
@section('css')
  <!-- start: Css -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/bootstrap.min.css')}}">

  <!-- plugins -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/datatables.bootstrap.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/font-awesome.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/animate.min.css')}}" />

  <link href="{{asset('asset/css/style.css')}}" rel="stylesheet">

  <link rel="shortcut icon" href="{{asset('asset/img/logomi.png')}}">

@endsection

@section('profile')
  <li class="user-name"><span>{{auth::user()->username}}</span></li>
                    <li class="dropdown avatar-dropdown">
                     <img src="{{asset('asset/img/avatar.jpg')}}" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
                     <ul class="dropdown-menu user-dropdown">
                       <li><a href="{{url('profile')}}"><span class="fa fa-user"></span> My Profile</a></li>
                       <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();"><span class="fa fa-power-off"></span> Logout</a></li>
                          {{-- Di panggil pada event onclick --}}
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>

                        </ul>
                      </li>
@endsection
@section('leftmenu')

@endsection
@section('content')
<div id="content">
  <!-- tampilan pilih penilai -->
<div class="tab-wrapper text-center">
  <div class="panel box-shadow-none text-left content-header">
    <div class="panel-body" style="padding-bottom:0px">
      <div class="col-md-12">
        <h3 class="animated fadeInLeft"> Pengajuan Kenaikan Pangkat</h3>
        <p class="animated fadeInDown">
            Pengajuan Kenaikan Pangkat  <span class="fa-angle-right fa"></span>  Pilih Penilai
                </p>
      </div>



    </div>
    <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-heading"><h3>Pilih Penilai </h3>
                    </div>
                    <div class="panel-body">
                      <div class="responsive-table">
                      <!-- tabel pengajuan yang masuk -->
                      <table id="tblist" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th hidden></th>
                          <th style="width:4%; text-align: center;">No</th>
                          <th style="width:17%; text-align: center;">Nama</th>
                          <th style="width:12%; text-align: center;">Jabatan Asal</th>
                          <th style="width:12%; text-align: center;">Jabatan Tujuan</th>
                          <th style="width:17%; text-align: center;">Penilai</th>
                          <th style="width:12%; text-align: center;">Link</th>
                          <th style="width:12%; text-align: center;">Status</th>
                          <th style="text-align: center;">Catatan</th>
                          <th style="width:12%; text-align: center;">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                          @php $no = 1; @endphp
                          @foreach ($nilai as $data)
                            <tr>
                              <td hidden>{{$data->id}}</td>
                              <td style="text-align: center;">{{ $no++ }}</td>
                              <td>{{ $data->nama }}</td>
                              <td>{{ $data->jabatan_asal }}</td>
                              <td>{{ $data->jabatan_tujuan }}</td>
                              <td>{{$data->nama_penilai}}</td>
                              <td><a href="https://{{ $data->link_drive }}"><label>link</label></td>
                              <td>
                                @if ($data->status == 'Terverifikasi')
                                <span class="btn btn-flat btn-info" style="width:130px; height:28px; pointer-events: none; border: 0px;">Terverifikasi</span>
                                @elseif ($data->status == 'Sedang Dinilai')
                                <span class="btn btn-flat btn-primary" style="width:130px; height:28px; pointer-events: none; border: 0px;">Sedang Dinilai</span>
                                @elseif ($data->status == 'Penilai Ditemukan')
                                <span class="btn btn-flat btn-primary" style="width:130px; height:28px; pointer-events: none; border: 0px;">Penilai Ditemukan</span>
                                @elseif ($data->status == 'Mencari Penilai')
                                <span class="btn btn-flat btn-default" style="width:130px; height:28px; pointer-events: none; border: 0px;">Mencari Penilai</span>
                                @elseif ($data->status == 'Menunggu Dinilai')
                                <span class="btn btn-flat btn-default" style="width:130px; height:28px; pointer-events: none; border: 0px;">Menunggu Dinilai</span>
                                @elseif ($data->status == 'Selesai')
                                <span class="btn btn-flat btn-success" style="width:130px; height:28px; pointer-events: none; border: 0px;">Selesai</span>
                                @endif
                              </td>

                              <td>{{ $data->catatan }}</td>
                              <td>
                                <button type="button" class="btn btn-circle btn-mn btn-success klik" data-toggle="modal" data-target="#modal">
                                <span class="fa fa-user" ></span></button>
                                <a class=" btn btn-circle btn-mn btn-primary" href="{{ url('/nilai-pak/'.$data->id) }}"><span class="fa fa-eye" style="padding-top: 7px;"></span></a>
                               </td>
                            </tr>
                          @endforeach
                      </tbody>
                        </table>
                      </div>

                  </div>
                  <!-- Modal -->
                                <!-- modal pilih penilai -->
                                <div id="modal" class="modal animated fadeInUp" >
                                  <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content" style="
                                          width: 1250px;
                                          border-left-width: 0px;
                                          left: -330;
                                      ">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Pilih Penilai - Lingkup Penilai</h4>

                                      </div>
                                      <ul id="tabs-demo" class="nav nav-tabs content-header-tab" role="tablist" style="padding-top:10px;">
                                            <li role="presentation" class="active">
                                                <a href="#panels-area-demo" id="tabs2" data-toggle="tab">Internal</a>
                                            </li>
                                            <li role="presentation" class="">
                                                <a href="#tabs-area-demo" id="tabs2" data-toggle="tab">Eksternal</a>
                                            </li>
                                        </ul>
                                      <div class="col-md-12 tab-content">

            {{-- START TAB PANE --}}
            <!-- penilai internal -->
            <div role="tabpanel" class="tab-pane fade active in" id="panels-area-demo" aria-labelledby="tabs1">
                        <div class="responsive-table">
                              <table id="tbpenilaiin" class="table table-striped table-bordered datatables-example" width="100%" cellspacing="0">
                                            <thead>
                                              <tr>
                                                <th style="width:4%; text-align: center;">No</th>
                                                <th style="width:10%; text-align: center;">NIP</th>
                                                <th style="width:10%; text-align: center;">NIDN</th>
                                                <th style="width:15%; text-align: center;">Nama</th>
                                                <th style="width:10%; text-align: center;">Rumpun</th>
                                                <th style="width:10%; text-align: center;">Sub Rumpun</th>
                                                <th style="width:10%; text-align: center;">Jabatan</th>
                                                <th style="width:10%; text-align: center;">Periode</th>
                                                <th style="width:3%; text-align: center;">Aksi</th>
                                              </tr>
                                            </thead>
                                          <tbody>
                                             @php $no = 1; @endphp
                                             @foreach ( $internal as $data)
                                                  <tr id="{{$data->id}}">
                                                    <td style="text-align: center;">{{ $no++ }}</td>
                                                    <td>{{ $data->nip }}</td>
                                                    <td>{{ $data->nidn }}</td>
                                                    <td>{{ $data->nama }}</td>
                                                    <td>{{ $data->rumpun }}</td>
                                                    <td>{{ $data->rumpun_sub }}</td>
                                                    <td>{{ $data->jafung }}</td>
                                                    <td>2018</td>
                                                    <td><button type="button" class="btn btn-circle btn-mn btn-primary internal" value="{{$data->id}}"><span class="fa fa-check"></span></button></td>
                                                  </tr>
                                              @endforeach
                                            </tbody>
                                        </table>

                                      </div>


            </div>
            {{-- END TAB PANE 1 --}}
            {{-- START PANEL 2 --}}
            <!-- penilai eksternal -->
            <div role="tabpanel" class="tab-pane fade" id="tabs-area-demo" aria-labelledby="tabs2">
              <div class="responsive-table">
                          <table id="tbpenilaieks" class="table table-striped table-bordered datatables-example" width="100%" cellspacing="0">
                                            <thead>
                                              <tr>
                                                <th hidden></th>
                                                <th style="width:4%; text-align: center;">No</th>
                                                <th style="width:10%; text-align: center;">NIP</th>
                                                <th style="width:10%; text-align: center;">NIDN</th>
                                                <th style="width:15%; text-align: center;">Nama</th>
                                                <th style="width:10%; text-align: center;">Rumpun</th>
                                                <th style="width:10%; text-align: center;">Sub Rumpun</th>
                                                <th style="width:10%; text-align: center;">Jabatan</th>
                                                <th style="width:10%; text-align: center;">Periode</th>
                                                <th style="width:3%;">Aksi</th>
                                              </tr>
                                            </thead>
                                          <tbody>
                                             @php $no = 1; @endphp
                                             @foreach ( $eksternal as $data)
                                                  <tr id="{{$data->id_penilai_eks}}">
                                                    <td hidden>{{$data->id_penilai_eks}}</td>
                                                    <td style="text-align: center;">{{ $no++ }}</td>
                                                    <td>{{ $data->nip }}</td>
                                                    <td>{{ $data->nidn }}</td>
                                                    <td>{{ $data->nama }}</td>
                                                    <td>{{ $data->rumpun }}</td>
                                                    <td>{{ $data->sub_rumpun }}</td>
                                                    <td>{{ $data->jabatan }}</td>
                                                    <td>2018</td>
                                                    <td><button type="button" class="btn btn-circle btn-mn btn-primary btneks" value="{{$data->id_penilai_eks}}"><span class="fa fa-check"></span></button></td>
                                                  </tr>
                                              @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                      </div>
                                      {{-- END TAB PANELS 2 --}}
                                  </div>

                                 <div class="modal-footer">

                                      </div>
                                    </div><!-- /.modal-content -->
                                  </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->






              </div>
            </div>


  </div>
@endsection
@section('javascript')
  <script src="{{ asset('asset/js/jquery.min.js') }}"></script>
  <script src="{{ asset('asset/js/jquery.ui.min.js') }}"></script>
  <script src="{{ asset('asset/js/bootstrap.min.js') }}"></script>
<!-- plugins -->
  <script src="{{ asset('asset/js/plugins/moment.min.js') }}"></script>
  <script src="{{ asset('asset/js/plugins/jquery.datatables.min.js') }}"></script>
  <script src="{{ asset('asset/js/plugins/datatables.bootstrap.min.js') }}"></script>
  <script src="{{ asset('asset/js/plugins/jquery.nicescroll.js') }}"></script>
<!-- custom -->
  <script src="{{ asset('asset/js/main.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#tblist').DataTable();
    $('#tbpenilaiin').DataTable();
    $('#tbpenilaieks').DataTable();
  });
</script>

<script type="text/javascript">
  var data ;
  $('#tblist').on('click', '.klik', function(event) {
    event.preventDefault();
     data = $(this).closest('tr').find('td:eq(1)').text();
     console.log(data);
    //alert(data);
    /* Act on the event */
  });

// penilai internal
$('#tbpenilaiin').on('click', '.internal', function(event) {
  event.preventDefault();
  var data2 = $(this).closest('tr').find('td:eq(0)').text();
  console.log(data2);
  var link = '{{url('insertpenilai')}}/'+data;
  //alert(link);
  //window.open(link,'_blank');
  //alert(link);
  //alert(data+data2);
  $.ajax({
    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
    url: link,
    type: 'POST',
    data : {
      'id': data2,
        _token: '{{csrf_token()}}'
    },
    success: function(data){
      console.log(data);
      alert('berhasil');
    },
    error:function(error){
      console.log(error);
      //alert(error);
    }
  })

  //alert(data);
});


  // Penilai eksternal
  $('#tbpenilaieks').on('click', '.btneks', function(event) {
    event.preventDefault();
    var data2 = $(this).closest('tr').find('td:eq(1)').text();
    console.log(data2);
    var link = '{{url('insertpenilai')}}/'+data;
    //alert(link);
    //window.open(link,'_blank');
    //alert(link);
    //alert(data+data2);
    $.ajax({
      headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
      url: link,
      type: 'POST',
      data : {
        'id': data2,
          _token: '{{csrf_token()}}'
      },
      success: function(data){
        console.log(data);
        alert('berhasil');
      },
      error:function(error){
        console.log(error);
        //alert(error);
      }
    })

    //alert(data);
  });
</script>
  <!-- end: Javascript -->
@endsection
