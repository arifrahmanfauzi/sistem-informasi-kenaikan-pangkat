@extends('layouts.template')
@section('css')
  <!-- start: Css -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/bootstrap.min.css')}}">

  <!-- plugins -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/datatables.bootstrap.min.css')}}"/>
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/font-awesome.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/animate.min.css')}}" />
  <link href="{{asset('asset/css/style.css')}}" rel="stylesheet">
  <!-- end: Css -->

  <!-- end: Css -->
  <!-- end: Css -->

  <link rel="shortcut icon" href="{{asset('asset/img/logomi.png')}}">

@endsection

@section('profile')
  <li class="user-name"><span>{{auth::user()->username}}</span></li>
                    <li class="dropdown avatar-dropdown">
                     <img src="{{asset('asset/img/avatar.jpg')}}" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
                     <ul class="dropdown-menu user-dropdown">
                       <li><a href="#"><span class="fa fa-user"></span> My Profile</a></li>
                       <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();"><span class="fa fa-power-off"></span> Logout</a></li>
                          {{-- Di panggil pada event onclick --}}
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>

                        </ul>
                      </li>
@endsection
@section('leftmenu')

@endsection
@section('content')
            <!-- Tambah penilai eksternal -->
            <div id="content">
              <div class="tab-wrapper text-center">
                <div class="panel box-shadow-none text-left content-header">
                  <div class="panel-body" style="padding-bottom:0px">
                    <div class="col-md-12">
                      <h3 class="animated fadeInLeft">Halaman Input Penilaian</h3>
                      <p class="animated fadeInDown">
                         Pengajuan Kenaikan Pangkat  <span class="fa-angle-right fa"></span>  Isi Nilai
                              </p>
                    </div>

                  </div>
                  <div class="col-md-12 top-20 padding-0">
                    <div class="col-md-12">
                        <div class="panel">
                          <div class="panel-heading"><h3>Tambah Nilai</h3>
                       </div>
                    <div class="panel-body">
                                       @foreach ($getrubrik as $data)
                                       <form id="form" class="" action="{{ route('penilaian.update', $data->id_rubrik) }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('PUT') }}
                                        <input type="hidden" name="id" value="{{ $data->id_rubrik }}">
                                        <input type="hidden" name="id_dupak" value="{{ $data->id_dupak }}">
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-2 control-label text-right" style="padding-top: 6px; font-size: 16px;">Nama Unsur</label>
                                         <div class="col-sm-8"><input type="text" class="form-control" id="nama_unsur" name="nama_unsur" value="{{ $data->nama_unsur }}"disabled></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-2 control-label text-right" style="padding-top: 6px; font-size: 16px;">Nama Sub Unsur</label>
                                         <div class="col-sm-8"><input type="text" class="form-control" id="nama_sub_unsur" name="nama_sub_unsur" disabled value="{{ $data->nama_sub_unsur }}"></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-2 control-label text-right" style="padding-top: 6px; font-size: 16px;">Nama Sub Bidang</label>
                                         <div class="col-sm-8"><input type="text" class="form-control" id="nama_sub_bidang" name="nama_sub_bidang" disabled value="{{ $data->nama_sub_bidang }}"></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-2 control-label text-right" style="padding-top: 6px; font-size: 16px;">Isian Rubrik</label>
                                        <div class="col-sm-8"><input type="text" class="form-control" id="isian_rubrik" name="isian_rubrik" disabled value="{{ $data->isian_rubrik }}"></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-2 control-label text-right" style="padding-top: 6px; font-size: 16px;">Tanggal Mulai</label>
                                        <div class="col-sm-8"><input type="text" class="form-control" id="tgl_mulai" name="tgl_mulai" disabled value="{{ $data->tanggal_mulai }}"></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-2 control-label text-right" style="padding-top: 6px; font-size: 16px;">Tanggal Berakhir Rubrik</label>
                                        <div class="col-sm-8"><input type="text" class="form-control" id="tgl_akhir" name="tgl_akhir" disabled value="{{ $data->tanggal_berakhir }}"></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-2 control-label text-right" style="padding-top: 6px; font-size: 16px;">Berkas</label>
                                        <div class="col-sm-8"><a href="https://{{ $data->berkas }}" class="col-sm-2 control-label text-left" style="padding-top: 6px; font-size: 16px;">{{ $data->berkas }}</a></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-2 control-label text-right" style="padding-top: 6px; font-size: 16px;">Angka Kredit Total</label>
                                        <div class="col-sm-8"><input type="text" class="form-control" id="kredit_total" name="kredit_total" disabled value="{{ $data->angka_kredit_total }}"></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-2 control-label text-right" style="padding-top: 6px; font-size: 16px;">Angka Kredit Penilai</label>
                                        <div class="col-sm-8"><input type="text" class="form-control" id="kredit_penilai" name="kredit_penilai" value="{{ $data->angka_kredit_penilai }}"></div>
                                        </div>
                                          <button type="reset" class="btn btn-default" data-dismiss="modal">Batal</button>
                                          <button type="submit" class="btn btn-primary">Nilai</button>
                                        
                                    </form>
                                    @endforeach
                </div>
              </div>

              </div>
            </div>
          </div>
                        <!-- end: content -->
@endsection
@section('javascript')
<!-- start: Javascript -->
  <script src="{{ asset('asset/js/jquery.min.js') }}"></script>
  <script src="{{ asset('asset/js/jquery.ui.min.js') }}"></script>
  <script src="{{ asset('asset/js/bootstrap.min.js') }}"></script>
<!-- plugins -->
  <script src="{{ asset('asset/js/plugins/moment.min.js') }}"></script>
  <script src="{{ asset('asset/js/plugins/jquery.datatables.min.js') }}"></script>
  <script src="{{ asset('asset/js/plugins/datatables.bootstrap.min.js') }}"></script>
  <script src="{{ asset('asset/js/plugins/jquery.nicescroll.js') }}"></script>
<!-- custom -->
  <script src="{{ asset('asset/js/main.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#datatables-example').DataTable();
  });
</script>
<!-- end: Javascript -->
@endsection
