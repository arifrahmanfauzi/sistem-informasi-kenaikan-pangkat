@extends('layouts.template')
@section('css')
  <!-- start: Css -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/bootstrap.min.css')}}">

  <!-- plugins -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/datatables.bootstrap.min.css')}}"/>
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/font-awesome.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/animate.min.css')}}" />

  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/select2.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/dropzone.css')}}" />

  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/bootstrap-material-datetimepicker.css')}}" />

  <link href="{{asset('asset/css/style.css')}}" rel="stylesheet">
  <!-- end: Css -->

  <!-- end: Css -->
  <link rel="stylesheet" href="{{asset('asset/date/css/datepicker.css')}}">
  <link rel="stylesheet" href="{{asset('asset/date/css/main.css')}}">
  <!-- end: Css -->

  <link rel="shortcut icon" href="{{asset('asset/img/logomi.png')}}">

@endsection

@section('profile')
  <li class="user-name"><span>{{auth::user()->username}}</span></li>
                    <li class="dropdown avatar-dropdown">
                     <img src="{{asset('asset/img/avatar.jpg')}}" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
                     <ul class="dropdown-menu user-dropdown">
                       <li><a href="#"><span class="fa fa-user"></span> My Profile</a></li>
                       <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();"><span class="fa fa-power-off"></span> Logout</a></li>
                          {{-- Di panggil pada event onclick --}}
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>

                        </ul>
                      </li>
@endsection
@section('leftmenu')

@endsection

@section('content')

      <!-- unggah sk -->
        <div id="content">
          <div class="panel box-shadow-none content-header">
            <div class="panel-body">
                <div class="col-md-12">
                    <h3 class="animated fadeInLeft">Unggah SK</h3>
                    <p class="animated fadeInDown">
                      Unggah <span class="fa-angle-right fa"></span> SK
                    </p>
                </div>
              </div>
          </div>

          <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-heading"><h3>Unggah SK Pengajuan </h3>
                    </div>
                    <div class="panel-body">
                    <form action="/file-upload" class="dropzone" id="my-awesome-dropzone"></form>
                  </div>
                </div>
              </div>
            </div>
@endsection
          
@section('javascript')
<!-- start: Javascript -->
  <script src="{{asset('asset/js/jquery.min.js')}}"></script>
  <script src="{{asset('asset/js/jquery.ui.min.js')}}"></script>
  <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>


<!-- plugins -->
 <script src="{{asset('asset/js/plugins/moment.min.js')}}"></script>
 <script src="{{asset('asset/js/plugins/dropzone.js')}}"></script>
 <script src="{{asset('asset/js/plugins/jquery.nicescroll.js')}}"></script>

<!-- custom -->
<script src="{{asset('asset/js/main.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function(){

  });
</script>
<!-- end: Javascript -->
@endsection