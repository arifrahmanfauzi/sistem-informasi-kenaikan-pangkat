@extends('layouts.template')
@section('css')
<!-- start: Css -->
<link rel="stylesheet" type="text/css" href="{{asset('asset/css/bootstrap.min.css')}}">

<!-- plugins -->
<link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/datatables.bootstrap.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/font-awesome.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/animate.min.css')}}" />

<link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/select2.min.css')}}" />

<link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/bootstrap-material-datetimepicker.css')}}" />

<link href="{{asset('asset/css/style.css')}}" rel="stylesheet">
<!-- end: Css -->

<!-- end: Css -->
<link rel="stylesheet" href="{{asset('asset/date/css/datepicker.css')}}">
<link rel="stylesheet" href="{{asset('asset/date/css/main.css')}}">
<!-- end: Css -->

<link rel="shortcut icon" href="{{asset('asset/img/logomi.png')}}">
<style>
.modal-backdrop.in {
opacity: 0.1;
}
</style>
@endsection

@section('profile')
<li class="user-name"><span>{{auth::user()->username}}</span></li>
<li class="dropdown avatar-dropdown">
    <img src="{{asset('asset/img/avatar.jpg')}}" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" />
    <ul class="dropdown-menu user-dropdown">
        <li><a href="#"><span class="fa fa-user"></span> My Profile</a></li>
        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();"><span class="fa fa-power-off"></span> Logout</a></li>
        {{-- Di panggil pada event onclick --}}
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>

    </ul>
</li>
@endsection
@section('leftmenu')

@endsection
@section('content')
<!-- tampilan menilai PAK -->
<div id="content">
    <div class="tabs-wrapper text-center">
        <div class="panel box-shadow-none text-left content-header">
            <div class="panel-body" style="padding-bottom:0px;">
                <div class="col-md-12">

                    <h3 class="animated fadeInLeft">Halaman Input Penilaian</h3>

                    <p class="animated fadeInDown">
                        Pengajuan Kenaikan Pangkat
                    </p>
                </div>
                <ul id="tabs-demo" class="nav nav-tabs content-header-tab" role="tablist" style="padding-top:10px;">
                    <li role="presentation">
                        <a href="#kesimpulan" id="tab3" data-toggle="tab">Dashboard</a>
                    </li>
                    <li role="presentation">
                        <a href="#hasilpak" id="tab2" data-toggle="tab">Penilaian</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-12 tab-content animated fadeInRight">
            {{-- START TAB PANE --}}

            {{-- END TAB PANE 1 --}}
            {{-- START PANEL 2 --}}
            <div role="tabpanel" class="tab-pane fade" id="panels-area-demo" aria-labelledby="tabs1">
                <div class="form-element">
                    <div class="col-md-12 padding-0">
                        <div class="col-md-5 hide">

                            <div class="form-group col-md-12" style="padding-top:30px;">
                                <div class="col-md-12 padding-0">
                                    <div id="noui-slider"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                    {{-- End Section Input! --}}

                </div>
            </div>


            <div type="text" id="noui-range" class="hide">
            </div>
            <!-- tab penilaian -->
            <div role="tabpanel" class="tab-pane" id="hasilpak" aria-labelledby="tab2">
                <div class="form-panel">
                    <div class="col-md-12 padding-0">
                        <div class="col-md-12 animated fadeInRight">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3>Penilaian</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="responsive-table">
                                        <!-- menampilkan tabel dupak -->
                                        <table id="tbhasil" class="table table-striped table-bordered" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th hidden></th>
                                                    <th style="text-align: center;">No</th>
                                                    <th style="text-align: center;">Nama Unsur</th>
                                                    <th style="text-align: center;">Nama Sub Unsur</th>
                                                    <th style="text-align: center;">Angka Kredit Total</th>
                                                    <th style="text-align: center;">Angka Kredit Penilai</th>
                                                    @if (checkPermission(['user']))
                                                        <th style="text-align: center;">Penilaian</th>
                                                    @endif
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php $no = 1; @endphp
                                                @foreach ($getrubrik as $rubrik )
                                                <tr id="{{ $rubrik->id_rubrik }}">
                                                    <td hidden>{{ $rubrik->id_dupak }}</td>
                                                    <td style="text-align: center;">{{ $no++ }}</td>
                                                    <td>{{ $rubrik->nama_unsur }}</td>
                                                    <td>{{ $rubrik->nama_sub_unsur }}</td>

                                                    <td style="text-align: center;">{{ $rubrik->angka_kredit_total }}</td>
                                                    <td style="text-align: center;">{{ $rubrik->angka_kredit_penilai }}</td>
                                                    @if (checkPermission(['user']))
                                                    <td>
                                                        <a class="btn btn-warning btn-circle btn-mn edit" href="{{ url('/isi-nilai/'.$rubrik->id_rubrik) }}"><span class="fa fa-pencil" style="padding-top: 7px;"></span> </a>
                                                    @endif

                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div>
                                        <br>
                                        <input type="text" class="form-control default" value='Total Angka Kredit: {{ $angka_total }}'>
                                        <br>
                                        <input type="text" class="form-control info" value='Total Angka Kredit dari Penilai: {{ $total_penilai }}'>
                                        <br>
                                        @if (checkPermission(['user']))
                                        <input type="button" class="btn btn-success" value="Simpan"/>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- tab dashboard -->
            <div class="tab-pane active in" id="kesimpulan" aria-labelledby="tab3">
                <div class="form-panel">
                    <div class="col-md-12 padding-0">
                        <div class="col-md-12 animated fadeInRight">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3>Dashboard</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12 panel-body">
                                        <!-- rincian -->
                                    @foreach ($getpengajuan as $data)
                                        <dl class="dl-horizontal">
                                            <input type="hidden" name="id" value="{{ $data->id }}">
                                            <dt style="font-size: 16;">Nama:</dt>
                                            <dd style="font-size: 16;">{{ $data->nama }}</dd>
                                            <br>
                                            <dt style="font-size: 16;">Jabatan Asal:</dt>
                                            <dd style="font-size: 16;">{{ $data->jabatan_asal }}</dd>
                                            <br>
                                            <dt style="font-size: 16;">Jabatan Tujuan:</dt>
                                            <dd style="font-size: 16;">{{ $data->jabatan_tujuan }}</dd>
                                            <br>
                                            <dt style="font-size: 16;">Kredit Total:</dt>
                                            <dd style="font-size: 16;">{{ $angka_total }}</dd>
                                            <br>
                                            <dt style="font-size: 16;">Kredit Penilai:</dt>
                                            <dd style="font-size: 16;">{{ $total_penilai }}</dd>
                                            <br>
                                            <dt style="font-size: 16;">Link Drive:</dt>
                                            <dd style="font-size: 16;"><a href="https://{{ $data->link_drive }}"><label>Link</label></a></dd>
                                            <br>
                                            <dt style="font-size: 16;">Status:</dt>
                                            <dd style="font-size: 16;">
                                                @if ($data->status == 'Diterima')
                                                <span class="btn btn-flat btn-info" style="width:130px; height:28px; pointer-events: none; border: 0px;">Direvisi</span>
                                                @elseif ($data->status == 'Sedang Dinilai')
                                                <span class="btn btn-flat btn-primary" style="width:130px; height:28px; pointer-events: none; border: 0px;">Sedang Dinilai</span>
                                                @elseif ($data->status == 'Penilai Ditemukan')
                                                <span class="btn btn-flat btn-primary" style="width:130px; height:28px; pointer-events: none; border: 0px;">Penilai Ditemukan</span>
                                                @elseif ($data->status == 'Mencari Penilai')
                                                <span class="btn btn-flat btn-default" style="width:130px; height:28px; pointer-events: none; border: 0px;">Mencari Penilai</span>
                                                @elseif ($data->status == 'Menunggu Dinilai')
                                                <span class="btn btn-flat btn-default" style="width:130px; height:28px; pointer-events: none; border: 0px;">Menunggu Dinilai</span>
                                                @else
                                                <span class="btn btn-flat btn-success" style="width:130px; height:28px; pointer-events: none; border: 0px;">Selesai</span>
                                                @endif
                                            </dd>
                                            @endforeach
                                      </dl>
                                      <!-- tabel kesimpulan -->
                              <div class="responsive-table">
                                                <table class="table table-striped table-bordered" width="100%" cellspacing="0">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: center;">No</th>
                                                            <th style="text-align: center;">Bidang</th>
                                                            <th style="text-align: center;">Kredit</th>
                                                            <th style="text-align: center;">Penilaian</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style="text-align: center;">1</td>
                                                            <td>Pendidikan</td>
                                                            <td style="text-align: center;">{{ $ak_pend }}</td>
                                                            <td style="text-align: center;">{{ $pen_pend }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: center;">2</td>
                                                            <td>Pelaksana Pendidikan</td>
                                                            <td style="text-align: center;">{{ $ak_pel }}</td>
                                                            <td style="text-align: center;">{{ $pen_pel }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: center;">3</td>
                                                            <td>Penelitian</td>
                                                            <td style="text-align: center;">{{ $ak_pene }}</td>
                                                            <td style="text-align: center;">{{ $pen_pene }}</td>
                                                        </tr>
                                                         <tr>
                                                            <td style="text-align: center;">4</td>
                                                            <td>Pengabdian</td>
                                                            <td style="text-align: center;">{{ $ak_peng }}</td>
                                                            <td style="text-align: center;">{{ $pen_peng }}</td>
                                                        </tr>
                                                         <tr>
                                                            <td style="text-align: center;">5</td>
                                                            <td>Penunjang</td>
                                                            <td style="text-align: center;">{{ $ak_pen }}</td>
                                                            <td style="text-align: center;">{{ $pen_pen }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>

</div>
</div>
</div>


@endsection
@section('javascript')
<!-- start: Javascript -->
<script src="{{asset('asset/js/jquery.min.js')}}"></script>
<script src="{{asset('asset/js/jquery.ui.min.js')}}"></script>
<script src="{{asset('asset/js/bootstrap.min.js')}}"></script>


<!-- plugins -->
<script src="{{asset('asset/js/plugins/moment.min.js')}}"></script>
<script src="{{asset('asset/js/plugins/jquery.knob.js')}}"></script>
<script src="{{asset('asset/js/plugins/ion.rangeSlider.min.js')}}"></script>
<script src="{{asset('asset/js/plugins/bootstrap-material-datetimepicker.js')}}"></script>

<script src="{{asset('asset/js/plugins/jquery.mask.min.js')}}"></script>
<script src="{{asset('asset/js/plugins/select2.full.min.js')}}"></script>
<script src="{{asset('asset/js/plugins/nouislider.min.js')}}"></script>
<script src="{{asset('asset/js/plugins/jquery.validate.min.js')}}"></script>
<script src="{{asset('asset/js/plugins/jquery.datatables.min.js')}}"></script>
<script src="{{asset('asset/js/plugins/datatables.bootstrap.min.js')}}"></script>
<script src="{{asset('asset/js/plugins/jquery.nicescroll.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#tbhasil').DataTable();
    });
</script>
<!-- custom -->
<script src="{{asset('asset/js/main.js')}}"></script>
<script src="{{ asset('asset/js/index.js') }}"></script>

<script src="{{ asset('js/vue.min.js') }}"></script>

<!-- end: Javascript -->
<!-- custom -->
<script src="{{asset('asset/datejs/datepicker.en-US.js')}}"></script>
{{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script> --}}
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://fengyuanchen.github.io/js/common.js"></script>
<script src="{{asset('asset/date/js/main.js')}}"></script>
<script src="{{asset('asset/date/js/datepicker.js')}}"></script>

<script src="{{asset('js/button-counter.js')}}"></script>
<script src="{{asset('js/custom-select.js')}}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
<script src="{{asset('js/input-pak.js')}}"></script>
<script src="{{asset('js/input-pak2.js')}}"></script>
<script>



    //alert(url);


    // $('#unsur').change(function() {
    //     var idunsur = $(this).val();
    //     //console.log(idunsur);
    //     if (idunsur) {
    //         $.ajax({
    //             type: "GET",
    //             url: "{{url('getsubunsur')}}?id_unsur=" + idunsur, //id_unsur ikuti sama di controller
    //             success: function(data) {
    //                 //alert(data)
    //                 //console.log(data);
    //                 if (data) {
    //                     $("#subunsur").empty(); //di kosongi dulu subunsur
    //                     $("#subunsur").append('<option>Select</option>');
    //                     $.each(data, function(value, key) {
    //                         $("#subunsur").append('<option value="' + key + '">' + value + '</option>');
    //                     });
    //
    //                 } else {
    //                     $("#subunsur").empty();
    //                 }
    //             }
    //         });
    //     } else {
    //         $("#subunsur").empty();
    //     }
    // });
</script>
<script>
    // $('#subunsur').change(function() {
    //     var idsubunsur = $(this).val();
    //     //console.log(idsubunsur);
    //     if (idsubunsur) {
    //         $.ajax({
    //             type: "GET",
    //             url: "{{url('getsubbidang')}}?id_sub_unsur=" + idsubunsur, //id_sub_unsur ikuti sama di controller
    //             success: function(data) {
    //                 //alert(data)
    //                 //console.log(data);
    //                 if (data) {
    //                     $("#subbidang").empty(); //di kosongi dulu subunsur
    //                     $("#subbidang").append('<option>Select</option>');
    //                     $.each(data, function(value, key) {
    //                         $("#subbidang").append('<option value="'+ key +'">'+ value +'</option>');
    //                     });
    //
    //                 } else {
    //                     $("#subbidang").empty();
    //                 }
    //             }
    //         });
    //     } else {
    //         $("#subbidang").empty();
    //     }
    // });
</script>
<!-- end: Javascript -->
@endsection
