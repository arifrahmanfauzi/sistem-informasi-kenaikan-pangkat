@extends('layouts.template')
@section('css')
  <!-- start: Css -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/bootstrap.min.css')}}">

  <!-- plugins -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/datatables.bootstrap.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/font-awesome.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/animate.min.css')}}" />

  <link href="{{asset('asset/css/style.css')}}" rel="stylesheet">

  <link rel="shortcut icon" href="{{asset('asset/img/logomi.png')}}">

@endsection

@section('profile')
  <li class="user-name"><span>{{auth::user()->username}}</span></li>
                    <li class="dropdown avatar-dropdown">
                     <img src="{{asset('asset/img/avatar.jpg')}}" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
                     <ul class="dropdown-menu user-dropdown">
                       <li><a href="#"><span class="fa fa-user"></span> My Profile</a></li>
                       <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();"><span class="fa fa-power-off"></span> Logout</a></li>
                          {{-- Di panggil pada event onclick --}}
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>

                        </ul>
                      </li>
@endsection
@section('leftmenu')

@endsection
@section('content')
<!-- tampilan penilai memilih pengajuan yang akan dinilai -->
<div id="content">
<div class="tab-wrapper text-center">
  <div class="panel box-shadow-none text-left content-header">
    <div class="panel-body" style="padding-bottom:0px">
      <div class="col-md-12">
        <h3 class="animated fadeInLeft"> Form Penilaian PAK</h3>
        <p class="animated fadeInDown">
            Penilaian  <span class="fa-angle-right fa"></span>  Penilaian PAK
                </p>
      </div>



    </div>
    <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-heading"><h3>Penilaian PAK </h3>

                    </div>
                    <div class="panel-body">
                      <div class="responsive-table">
                      <!-- tabel pengajuan -->
                      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th hidden></th>
                          <th style="width:4%; text-align: center;">No</th>
                          <th style="width:30%; text-align: center;">Nama</th>
                          <th style="width:15%; text-align: center;">Jabatan Asal</th>
                          <th style="width:15%; text-align: center;">Jabatan Tujuan</th>
                          <th style="width:13%; text-align: center;">Link</th>
                          <th style="width:13%; text-align: center;">Status</th>
                          <th style="width:10%; text-align: center;">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                       @php $no = 1; @endphp 
                          @foreach ( $penilaian as $data)
                          <tr>
                            <td hidden>{{$data->id}}</td>
                            <td style="text-align: center;">{{ $no++ }}</td>
                            <td>{{ $data->nama }}</td>
                            <td>{{ $data->jabatan_asal }}</td>
                            <td>{{ $data->jabatan_tujuan }}</td>
                            <td><a href="https://{{ $data->link_drive }}">Link</a></td>
                            <td>
                                @if ($data->status == 'Sedang Dinilai')
                                <span class="btn btn-flat btn-primary" style="width:130px; height:28px; pointer-events: none; border: 0px;">Sedang Dinilai</span>
                                @else
                                <span class="btn btn-flat btn-default" style="width:130px; height:28px; pointer-events: none; border: 0px;">Menunggu Dinilai</span>
                                @endif
                            </td>
                            <td>
                              <a class="btn btn-circle btn-mn btn-primary" href="{{ url('/nilai-pak/'.$data->id) }}"> <span class="fa fa-pencil" style="padding-top: 7px;"></span> </a>
                               <button type="button" class="btn btn-circle btn-mn btn-success"><span class="fa fa-check" ></span></button>
                            </td>
                            </tr>
                          @endforeach
                         </tbody>
                        </table>
                      </div>
                  </div>
                  <!-- Modal -->
                </div>
              </div>
              </div>
            </div>
  </div>
</div>
</div>
@endsection
@section('javascript')
  <script src="{{ asset('asset/js/jquery.min.js') }}"></script>
  <script src="{{ asset('asset/js/jquery.ui.min.js') }}"></script>
  <script src="{{ asset('asset/js/bootstrap.min.js') }}"></script>
<!-- plugins -->
  <script src="{{ asset('asset/js/plugins/moment.min.js') }}"></script>
  <script src="{{ asset('asset/js/plugins/jquery.datatables.min.js') }}"></script>
  <script src="{{ asset('asset/js/plugins/datatables.bootstrap.min.js') }}"></script>
  <script src="{{ asset('asset/js/plugins/jquery.nicescroll.js') }}"></script>
  <script type="text/javascript">
  $(document).ready(function(){
    $('#datatables-example').DataTable();
  });
</script>
<!-- custom -->
<script src="{{ asset('asset/js/main.js') }}"></script>

@endsection