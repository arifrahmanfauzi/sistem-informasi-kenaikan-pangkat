<table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Jabatan Asal</th>
            <th>Jabatan Tujuan</th>
            <th>Link Berkas</th>
            <th>Status</th>
            <th>catatan</th>
            
        </tr>
    </thead>
    <tbody>

        @foreach ( $pengajuan as $data)
        <tr>
            <td>{{ $data->id }}</td>
            <td>{{ $data->nama }}</td>
            <td>{{ $data->jabatan_asal }}</td>
            <td>{{ $data->jabatan_tujuan }}</td>
            <td><a href="https://{{ $data->link_drive }}"><label>link</label></td>
            <td>{{ $data->status }}</td>
            <td>{{ $data->catatan }}</td>

        </tr>
        @endforeach




    </tbody>
</table>
