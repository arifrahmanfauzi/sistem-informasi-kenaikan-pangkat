@extends('layouts.template')
@section('css')
  <!-- start: Css -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/bootstrap.min.css')}}">

  <!-- plugins -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/simple-line-icons.css')}}"/>

  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/font-awesome.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/animate.min.css')}}" />

  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/select2.min.css')}}" />



  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/bootstrap-material-datetimepicker.css')}}" />
  <link href="{{asset('asset/css/style.css')}}" rel="stylesheet">
  <!-- end: Css -->

  <!-- end: Css -->
  <link rel="stylesheet" href="{{asset('asset/date/css/datepicker.css')}}">
  <link rel="stylesheet" href="{{asset('asset/date/css/main.css')}}">
  <!-- end: Css -->

  <link rel="shortcut icon" href="{{asset('asset/img/logomi.png')}}">
<style>
    .modal-backdrop.in {
  opacity: 0.1;
  }
  </style>
@endsection
@section('profile')
<li class="user-name"><span>{{auth::user()->username}}</span></li>
<li class="dropdown avatar-dropdown">
    <img src="{{asset('asset/img/avatar.jpg')}}" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" />
    <ul class="dropdown-menu user-dropdown">
        <li><a href="{{route('profile')}}"><span class="fa fa-user"></span> My Profile</a></li>
        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"><span class="fa fa-power-off"></span> Logout</a></li>
        {{-- Di panggil pada event onclick --}}
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>

    </ul>
</li>

@endsection

@section('lefmenu')

@endsection
@section('content')
<!-- start: Content -->
<div id="content" class="profile-v1">
    <div class="col-md-12" style="top:20px">
      <div class="col-md-8">
                    <div class="panel form-element-padding">
                      <div class="panel-heading">
                       <h4>Profile</h4>
                      </div>
                       <div class="panel-body" style="padding-bottom:30px;">
                         @foreach ($nama as $data )


                        <div class="col-md-12">
                          <div class="form-group"><label class="col-sm-2 control-label text-right">Nama</label>
                            <div class="col-sm-10"><input type="text" class="form-control" placeholder="{{$data->nama}}"></div>
                            {{-- {{$nama}} --}}
                          </div><div class="form-group"><label class="col-sm-2 control-label text-right">NIP</label>
                            <div class="col-sm-10"><input type="text" class="form-control" placeholder="{{$data->nip}}"></div>
                          </div>

                          <div class="form-group"><label class="col-sm-2 control-label text-right">Kartu</label>
                            <div class="col-sm-10"><input type="text" class="form-control" placeholder="Kartu Pegawai"></div>
                          </div>
                          <div class="form-group"><label class="col-sm-2 control-label text-right">Tanggal Lahir</label>
                            <div class="col-sm-10"><input type="text" class="form-control" placeholder="tanggal lahir"></div>
                          </div>
                          <div class="form-group"><label class="col-sm-2 control-label text-right">Jenis Kelamin</label>
                            <div class="col-sm-8"><input type="text" class="form-control" placeholder="Jenis Kelamin"></div>
                          </div>
                          <div class="form-group"><label class="col-sm-2 control-label text-right">Pendidkan</label>
                            <div class="col-sm-8"><input type="text" class="form-control" placeholder="Pendidikan yang di perhitungkan angka kreditnya"></div>
                          </div>
                          <div class="form-group"><label class="col-sm-2 control-label text-right">Jabatan</label>
                            <div class="col-sm-8"><input type="text" class="form-control" placeholder="Jabtan Akademik Dosen"></div>
                          </div>
                          <div class="form-group"><label class="col-sm-2 control-label text-right">Masa Kerja</label>
                            <div class="col-sm-8"><input type="text" class="form-control" placeholder="masa kerja golongan lama"></div>
                          </div>
                          <div class="form-group"><label class="col-sm-2 control-label text-right">Masa Kerja baru</label>
                            <div class="col-sm-8"><input type="text" class="form-control" placeholder="Masa Kerja Baru"></div>
                          </div>
                          <div class="form-group"><label class="col-sm-2 control-label text-right">Unit Kerja</label>
                            <div class="col-sm-8"><input type="text" class="form-control" placeholder="Unit Kerja"></div>
                          </div>




                        </div>
                      @endforeach
                      </div>
                    </div>
                  </div>
        <div class="col-md-4">
            <div class="col-md-12 padding-0">
                <div class="panel box-v2">
                    <div class="panel-heading padding-0">
                        <img src="{{asset('asset/img/avatar.jpg')}}" class="box-v2-cover img-responsive">

                    </div>
                    <div class="panel-body bg-blue">
                        <div class="col-md-12 padding-0 text-center">
                            <div class="col-md-6 col-sm-6 col-xs-6 padding-0" style="width:100%;">
                                <h3>Fulan bin Fulan</h3>
                                <br>
                                <kbd>Kaprodi SI</kbd>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 padding-0">

            </div>

            <div class="col-md-12 padding-0">
                <div class="panel bg-light-blue">
                    <div class="panel-body text-white">
                        <p class="animated fadeInUp quote">Lorem ipsum dolor sit amet, consectetuer adipiscing elit Ut wisi..."</p>
                        <div class="col-md-12 padding-0">
                            <div class="text-left col-md-7 col-xs-12 col-sm-7 padding-0">
                                <span class="fa fa-twitter fa-2x"></span>
                                <span>22 May, 2015 via mobile</span>
                            </div>
                            <div style="padding-top:8px;" class="text-right col-md-5 col-xs-12 col-sm-5 padding-0">
                                <span class="fa fa-retweet"></span> 2000
                                <span class="fa fa-star"></span> 3000
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- end: content -->
@endsection
@section('javascript')
  <!-- start: Javascript -->
  <script src="{{asset('asset/js/jquery.min.js')}}"></script>
  <script src="{{asset('asset/js/jquery.ui.min.js')}}"></script>
  <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>


  <!-- plugins -->
  <script src="{{asset('asset/js/plugins/moment.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.knob.js')}}"></script>
  <script src="{{asset('asset/js/plugins/ion.rangeSlider.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/bootstrap-material-datetimepicker.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.nicescroll.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.mask.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/select2.full.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/nouislider.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.validate.min.js')}}"></script>
  <!-- custom -->
  <script src="{{asset('asset/js/main.js')}}"></script>
  <!-- end: Javascript -->
  <!-- custom -->
   <script src="{{asset('asset/datejs/datepicker.en-US.js')}}"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
  <script src="https://fengyuanchen.github.io/js/common.js"></script>
  <script src="{{asset('asset/date/js/main.js')}}"></script>
  <script src="{{asset('asset/date/js/datepicker.js')}}"></script>
  <!-- end: Javascript -->
@endsection
