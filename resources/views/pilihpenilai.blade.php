@extends('layouts.template')
@section('css')
  <!-- start: Css -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/bootstrap.min.css')}}">

  <!-- plugins -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/simple-line-icons.css')}}"/>

  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/font-awesome.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/animate.min.css')}}" />

  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/select2.min.css')}}" />



  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/bootstrap-material-datetimepicker.css')}}" />
  <link href="{{asset('asset/css/style.css')}}" rel="stylesheet">
  <!-- end: Css -->

  <!-- end: Css -->
  <link rel="stylesheet" href="{{asset('asset/date/css/datepicker.css')}}">
  <link rel="stylesheet" href="{{asset('asset/date/css/main.css')}}">
  <!-- end: Css -->

  <link rel="shortcut icon" href="{{asset('asset/img/logomi.png')}}">

@endsection

@section('profile')
  <li class="user-name"><span>{{auth::user()->username}}</span></li>
  <li class="dropdown avatar-dropdown">
      <img src="{{asset('asset/img/avatar.jpg')}}" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" />
      <ul class="dropdown-menu user-dropdown">
          <li><a href="{{route('profile')}}"><span class="fa fa-user"></span> My Profile</a></li>
          <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();"><span class="fa fa-power-off"></span> Logout</a></li>
          {{-- Di panggil pada event onclick --}}
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>

      </ul>
  </li>

@endsection
@section('leftmenu')

@endsection
@section('content')
<div id="content">
<div class="tab-wrapper text-center">
  <div class="panel box-shadow-none text-left content-header">
    <div class="panel-body" style="padding-bottom:0px">
      <div class="col-md-12">
        <h3 class="animated fadeInLeft"> Pengajuan Kenaikan Pangkat</h3>
        <p class="animated fadeInDown">
            Pengajuan Kenaikan Pangkat  <span class="fa-angle-right fa"></span>  Pilih Penilai
                </p>
      </div>



    </div>
    <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-heading"><h3>Pilih Penilai </h3>
                    </div>
                    <div class="panel-body">
                      <div class="responsive-table">
                      <table class="table table-striped table-bordered datatables-example" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th style="width:4%;">No</th>
                          <th style="width:17%;">Nama</th>
                          <th style="width:12%;">Jabatan Asal</th>
                          <th style="width:12%;">Jabatan Tujuan</th>
                          <th style="width:17%;">Penilai</th>
                          <th style="width:12%;">Link</th>
                          <th style="width:12%;">Status</th>
                          <th>Catatan</th>
                          <th style="width:12%;">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                          @php $no = 1; @endphp
                          @foreach ($penilaian as $data)
                            <tr>
                              <td style="text-align: center;">{{ $no++ }}</td>
                              <td hidden>{{ $data->id}}</td>
                              <td>{{ $data->nama }}</td>
                              <td>{{ $data->jabatan_asal }}</td>
                              <td>{{ $data->jabatan_tujuan }}</td>
                              <td><a href="https://{{ $data->link_drive }}"><label>link</label></td>
                              <td>{{ $data->status }}</td>
                              <td>
                                @if ($data->status == 'Diterima')
                                <span class="btn btn-flat btn-info" style="width:130px; height:28px; pointer-events: none; border: 0px;">Diterima</span>
                                @elseif ($data->status == 'Sedang Dinilai')
                                <span class="btn btn-flat btn-primary" style="width:130px; height:28px; pointer-events: none; border: 0px;">Sedang Dinilai</span>
                                @elseif ($data->status == 'Penilai Ditemukan')
                                <span class="btn btn-flat btn-primary" style="width:130px; height:28px; pointer-events: none; border: 0px;">Penilai Ditemukan</span>
                                @elseif ($data->status == 'Mencari Penilai')
                                <span class="btn btn-flat btn-default" style="width:130px; height:28px; pointer-events: none; border: 0px;">Mencari Penilai</span>
                                @elseif ($data->status == 'Menunggu Dinilai')
                                <span class="btn btn-flat btn-default" style="width:130px; height:28px; pointer-events: none; border: 0px;">Menunggu Dinilai</span>
                                @else
                                <span class="btn btn-flat btn-success" style="width:130px; height:28px; pointer-events: none; border: 0px;">Selesai</span>
                                @endif
                              </td>
                              <td>{{ $data->catatan }}</td>
                              <td>
                               @if ($data->status == 'Sedang Dinilai'||$data->status == 'Penilai Ditemukan'||$data->status == 'Menunggu Dinilai')
                                <button type="button" class="btn btn-circle btn-mn btn-success" data-toggle="modal" data-target="#modal" disabled>
                                <span class="fa fa-user" ></span></button>
                                 <a class=" btn btn-circle btn-mn btn-primary" href="{{ url('/pak2') }}"><span class="fa fa-eye" style="padding-top: 7px;"></span></a>
                                @elseif ($data->status == 'Selesai')
                                <button type="button" class="btn btn-circle btn-mn btn-success" data-toggle="modal" data-target="#modal" disabled> <span class="fa fa-user" ></span></button>
                                <button type="button" class="btn btn-circle btn-mn btn-danger"><span class="fa fa-print" ></span></button>
                                 <a class=" btn btn-circle btn-mn btn-primary" href="{{ url('/pak2') }}"><span class="fa fa-eye" style="padding-top: 7px;"></span></a>
                                @else
                               <button type="button" class="btn btn-circle btn-mn btn-success" data-toggle="modal" data-target="#modal">
                               <span class="fa fa-user" ></span></button>
                               <a class=" btn btn-circle btn-mn btn-primary" href="{{ url('/pak2') }}"><span class="fa fa-eye" style="padding-top: 7px;"></span></a>
                                @endif
                               </td>
                            </tr>
                          @endforeach
                      </tbody>
                        </table>
                      </div>

                  </div>
                  <!-- Modal -->

                                <div id="modal" class="modal animated fadeInUp" >
                                  <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content" style="
                                          width: 1250px;
                                          border-left-width: 0px;
                                          left: -330;
                                      ">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Pilih Penilai - Lingkup Penilai</h4>

                                      </div>
                                      <ul id="tabs-demo" class="nav nav-tabs content-header-tab" role="tablist" style="padding-top:10px;">
                                            <li role="presentation" class="active">
                                                <a href="#panels-area-demo" id="tabs2" data-toggle="tab">Internal</a>
                                            </li>
                                            <li role="presentation" class="">
                                                <a href="#tabs-area-demo" id="tabs2" data-toggle="tab">Eksternal</a>
                                            </li>
                                        </ul>
                                      <div class="col-md-12 tab-content">

            {{-- START TAB PANE --}}
            <div role="tabpanel" class="tab-pane fade active in" id="panels-area-demo" aria-labelledby="tabs1">
                        <div class="responsive-table">
                              <table class="table table-striped table-bordered datatables-example" width="100%" cellspacing="0">
                                            <thead>
                                              <tr>
                                                <th style="width:4%">No</th>
                                                <th style="width:10%;">NIP</th>
                                                <th style="width:10%;">NIDN</th>
                                                <th style="width:15%;">Nama</th>
                                                <th style="width:10%;">Rumpun</th>
                                                <th style="width:10%;">Sub Rumpun</th>
                                                <th style="width:10%;">Jabatan</th>
                                                <th style="width:10%;">Periode</th>
                                                <th style="width:10%;">Jumlah</th>
                                                <th style="width:3%;">Aksi</th>
                                              </tr>
                                            </thead>
                                          <tbody>
                                             @php $no = 1; @endphp
                                             @foreach ( $internal as $data)
                                                  <tr>
                                                    <td style="text-align: center;">{{ $no++ }}</td>
                                                    <td>{{ $data->nip }}</td>
                                                    <td>{{ $data->nidn }}</td>
                                                    <td>{{ $data->nama }}</td>
                                                    <td>{{ $data->rumpun }}</td>
                                                    <td>{{ $data->rumpun_sub }}</td>
                                                    <td>{{ $data->jafung }}</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td><button type="button" class="btn btn-circle btn-mn btn-primary"><span class="fa fa-check"></span></button></td>
                                                  </tr>
                                              @endforeach
                                            </tbody>
                                        </table>

                                      </div>


            </div>
            {{-- END TAB PANE 1 --}}
            {{-- START PANEL 2 --}}
            <div role="tabpanel" class="tab-pane fade" id="tabs-area-demo" aria-labelledby="tabs2">
              <div class="responsive-table">

                                             <table class="table table-striped table-bordered datatables-example" width="100%" cellspacing="0">
                                            <thead>
                                              <tr>
                                                <th style="width:4%">No</th>
                                                <th style="width:10%;">NIP</th>
                                                <th style="width:10%;">NIDN</th>
                                                <th style="width:15%;">Nama</th>
                                                <th style="width:10%;">Rumpun</th>
                                                <th style="width:10%;">Sub Rumpun</th>
                                                <th style="width:10%;">Jabatan</th>
                                                <th style="width:10%;">Periode</th>
                                                <th style="width:10%;">Jumlah</th>
                                                <th style="width:3%;">Aksi</th>
                                              </tr>
                                            </thead>
                                          <tbody>
                                             @php $no = 1; @endphp
                                             @foreach ( $eksternal as $data)
                                                  <tr>
                                                    <td style="text-align: center;">{{ $no++ }}</td>
                                                    <td>{{ $data->nip }}</td>
                                                    <td>{{ $data->nidn }}</td>
                                                    <td>{{ $data->nama }}</td>
                                                    <td>{{ $data->rumpun }}</td>
                                                    <td>{{ $data->sub_rumpun }}</td>
                                                    <td>{{ $data->jabatan }}</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td><button type="button" class="btn btn-circle btn-mn btn-primary"><span class="fa fa-check"></span></button></td>
                                                  </tr>
                                              @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                      </div>
                                      {{-- END TAB PANELS 2 --}}
                                  </div>

                                 <div class="modal-footer">

                                      </div>
                                    </div><!-- /.modal-content -->
                                  </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->






              </div>
            </div>


  </div>
@endsection
@section('javascript')
  <!-- start: Javascript -->
  <script src="{{asset('asset/js/jquery.min.js')}}"></script>
  <script src="{{asset('asset/js/jquery.ui.min.js')}}"></script>
  <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>


  <!-- plugins -->
  <script src="{{asset('asset/js/plugins/moment.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.knob.js')}}"></script>
  <script src="{{asset('asset/js/plugins/ion.rangeSlider.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/bootstrap-material-datetimepicker.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.nicescroll.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.mask.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/select2.full.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/nouislider.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.validate.min.js')}}"></script>
  <!-- custom -->
  <script src="{{asset('asset/js/main.js')}}"></script>
  <!-- end: Javascript -->
  <!-- custom -->
   <script src="{{asset('asset/datejs/datepicker.en-US.js')}}"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
  <script src="https://fengyuanchen.github.io/js/common.js"></script>
  <script src="{{asset('asset/date/js/main.js')}}"></script>
  <script src="{{asset('asset/date/js/datepicker.js')}}"></script>
  <!-- end: Javascript -->
<script type="text/javascript">
  $(document).ready(function(){
    $('.datatables-example').DataTable();
  });
</script>
  <!-- end: Javascript -->
@endsection
