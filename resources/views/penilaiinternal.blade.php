@extends('layouts.template')
@section('css')
  <!-- start: Css -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/bootstrap.min.css')}}">

  <!-- plugins -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/datatables.bootstrap.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/font-awesome.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/animate.min.css')}}" />

  <link href="{{asset('asset/css/style.css')}}" rel="stylesheet">


  <link rel="shortcut icon" href="{{asset('asset/img/logomi.png')}}">

@endsection

@section('profile')
  <li class="user-name"><span>{{auth::user()->username}}</span></li>
  <li class="dropdown avatar-dropdown">
      <img src="{{asset('asset/img/avatar.jpg')}}" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" />
      <ul class="dropdown-menu user-dropdown">
          <li><a href="{{route('profile')}}"><span class="fa fa-user"></span> My Profile</a></li>
          <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();"><span class="fa fa-power-off"></span> Logout</a></li>
          {{-- Di panggil pada event onclick --}}
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>

      </ul>
  </li>

@endsection
@section('leftmenu')

@endsection
@section('content')
<div id="content">
<div class="tab-wrapper text-center">
  <div class="panel box-shadow-none text-left content-header">
    <div class="panel-body" style="padding-bottom:0px">
      <div class="col-md-12">
        <h3 class="animated fadeInLeft"> Data Penilai Internal</h3>
        <p class="animated fadeInDown">
            Penilai  <span class="fa-angle-right fa"></span>  Penilai Internal
                </p>
      </div>



    </div>
    <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-heading"><h3>Penilai Internal </h3>

                    </div>
                    <div class="panel-body">

                      <div class="responsive-table">
                          <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                              <tr>
                                <th style="width:4%;">No</th>
                                <th style="width:14%;">NIP</th>
                                <th style="width:10%;">NIDN</th>
                                <th style="width:20%;">Nama</th>
                                <th style="width:14%;">Unit Kerja</th>
                                <th style="width:10%;">Rumpun</th>
                                <th style="width:10%;">Sub Rumpun</th>
                                <th style="width:10%;">Jabatan Fungsional</th>
                              </tr>
                            </thead>
                          <tbody>
                             @php $no = 1; @endphp
                             @foreach ( $internal as $data)
                                  <tr id="{{$data->id}}">
                                    <td style="text-align: center;">{{ $no++ }}</td>
                                    <td>{{ $data->nip }}</td>
                                    <td>{{ $data->nidn }}</td>
                                    <td>{{ $data->nama }}</td>
                                    <td>{{ $data->unit_kerja }}</td>
                                    <td>{{ $data->rumpun }}</td>
                                    <td>{{ $data->rumpun_sub }}</td>
                                    <td>{{ $data->jafung }}</td>
                                  </tr>
                              @endforeach
                            </tbody>
                        </table>
                      </div>

                  <!-- Modal -->




                </div>


              </div>

              </div>
            </div>


  </div>


</div>
</div>
@endsection
@section('javascript')
  <!-- start: Javascript -->
  <script src="{{asset('asset/js/jquery.min.js')}}"></script>
  <script src="{{asset('asset/js/jquery.ui.min.js')}}"></script>
  <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>


  <!-- plugins -->
  <script src="{{asset('asset/js/plugins/moment.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.knob.js')}}"></script>
  <script src="{{asset('asset/js/plugins/ion.rangeSlider.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/bootstrap-material-datetimepicker.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.nicescroll.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.mask.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/select2.full.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/nouislider.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.validate.min.js')}}"></script>
  <!-- custom -->
  <script src="{{asset('asset/js/main.js')}}"></script>
  <!-- end: Javascript -->
  <!-- custom -->
   <script src="{{asset('asset/datejs/datepicker.en-US.js')}}"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
  <script src="https://fengyuanchen.github.io/js/common.js"></script>
  <script src="{{asset('asset/date/js/main.js')}}"></script>
  <script src="{{asset('asset/date/js/datepicker.js')}}"></script>
  <!-- end: Javascript -->
<script type="text/javascript">
  $(document).ready(function(){
    $('#datatables-example').DataTable();
  });
</script>
<!-- end: Javascript -->
@endsection
