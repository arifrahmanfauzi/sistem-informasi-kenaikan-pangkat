@extends('layouts.template')
@section('css')
<!-- start: Css -->
<link rel="stylesheet" type="text/css" href="{{asset('asset/css/bootstrap.min.css')}}">

<!-- plugins -->
<link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/datatables.bootstrap.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/font-awesome.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/animate.min.css')}}" />

<link href="{{asset('asset/css/style.css')}}" rel="stylesheet">

<link rel="shortcut icon" href="{{asset('asset/img/logomi.png')}}">

@endsection
@section('profile')
<li class="user-name"><span>{{auth::user()->username}}</span></li>
<li class="dropdown avatar-dropdown">
    <img src="{{asset('asset/img/avatar.jpg')}}" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" />
    <ul class="dropdown-menu user-dropdown">
        <li><a href="{{url('profile')}}"><span class="fa fa-user"></span> My Profile</a></li>
        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();"><span class="fa fa-power-off"></span> Logout</a></li>
        {{-- Di panggil pada event onclick --}}
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>

    </ul>
</li>
@endsection
@section('leftmenu')

@endsection
@section('content')
<div id="content">
    <div class="panel box-shadow-none text-left content-header">
        <div class="panel box-shadow-none text-left content-header">
            <div class="panel-body" style="padding-bottom:0px">
                <div class="col-md-12">
                    <h3 class="animated fadeInLeft"> Data Pengaju Jabatan</h3>

                </div>
            </div>
            <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3>Data Pengaju Jabatan</h3>
                        </div>
                        <div class="panel-body">
                            <div class="responsive-table">
                                <table id="tblist" class="table table-striped table-bordered" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama dosen</th>
                                            <th>Jabatan Asal</th>
                                            <th>Jabatan Tujuan</th>
                                            <th>Link</th>
                                            <th>Status</th>
                                            <th>Catatan</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            @php $no = 1;
                                            @endphp
                                            @foreach ( $data as $fakultas)
                                        <tr>
                                            <td hidden>{{ $fakultas->id }}</td>
                                            <td>{{$no++}}</td>
                                            <td>{{ $fakultas->nama }}</td>
                                            <td>{{ $fakultas->jabatan_asal }}</td>
                                            <td>{{ $fakultas->jabatan_tujuan }}</td>
                                            <td>{{ $fakultas->link_drive }}></td>
                                            <td>{{ $fakultas->status }}</td>
                                            <td>{{ $fakultas->catatan }}</td>
                                            <td>
                                                <button type="button" class="btn btn-success box-shadow-none btn-circle btn-mn ubah"><span class="glyphicon glyphicon-edit"></button>

                                                <a class="btn btn-secondary active btn-circle btn-mn" href="https://{{ $fakultas->link_drive }}" role="button" target="_blank"><span class="glyphicon glyphicon-open"></span></a>

                                                <a href="{{url('/suket/'.$fakultas->id)}}" target="_blank">
                                                    <span class="glyphicon glyphicon-download-alt"></span>
                                                </a>

                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="modal fade" id="modalubah" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">Edit Status</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                        <div class="modal-body">
                                          <form id="formubah">
                                            @csrf
                                            <input id="idpengajuan" type="text" name="id" value="" hidden>
                                            <div class="form-group" style="padding-bottom:5px;">
                                                <label class="col-sm-2 control-label text-right">Nama</label>
                                                <div class="col-sm-10"><input id="nama" type="text" value="" class="form-control" disabled>
                                                </div>
                                            </div>
                                            <div class="panel-body" style="padding-top:5px; padding-bottom:5px;"></div>
                                            <div class="form-group" style="padding-bottom:5px;">
                                                <label class="col-sm-2 control-label text-right">Jabatan Asal</label>
                                                <div class="col-sm-10"><input id="ja" type="text" value="" class="form-control" disabled>
                                                </div>
                                            </div>
                                            <div class="panel-body" style="padding-top:5px; padding-bottom:5px;"></div>
                                            <div class="form-group" style="padding-bottom:10px;">
                                                <label class="col-sm-2 control-label text-right">Jabatan Tujuan</label>
                                                <div class="col-sm-10"><input id="jt" type="text" value="" class="form-control" disabled>
                                                </div>
                                            </div>

                                            <div class="panel-body" style="padding-top:5px; padding-bottom:5px;"></div>

                                            <div class="panel-body" style="padding-top:5px; padding-bottom:5px;"></div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-right">Status</label>
                                                <select id="status" class="custom-select" style="margin-left:19px">

                                                    <option value="1">Terverifikasi</option>
                                                    <option value="2">Di revisi</option>

                                                </select>
                                                <input id="instatus" type="text" name="status" value="" hidden>
                                            </div>
                                            <div class="panel-body" style="padding-top:5px; padding-bottom:5px;"></div>
                                            <div class="form-group"><label class="col-sm-2 control-label text-right">Catatan</label>
                                                <div class="col-sm-10"><input type="text" class="form-control" name="catatan" value=""></div>
                                            </div>
                                            <div class="panel-body" style="padding-top:5px; padding-bottom:5px;"></div>
                                          </form>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button id="save" type="button" class="btn btn-primary" data-dismiss="modal">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
</div>
@endsection
@section('javascript')
  <!-- start: Javascript -->
  <script src="{{asset('asset/js/jquery.min.js')}}"></script>
  <script src="{{asset('asset/js/jquery.ui.min.js')}}"></script>
  <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>


  <!-- plugins -->
  <script src="{{asset('asset/js/plugins/moment.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.knob.js')}}"></script>
  <script src="{{asset('asset/js/plugins/ion.rangeSlider.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/bootstrap-material-datetimepicker.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.nicescroll.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.mask.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/select2.full.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/nouislider.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.validate.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.datatables.min.js')}}"></script>
  <script src="{{ asset('js/pengajuan.js') }}"></script>
  <script src="{{asset('asset/js/plugins/datatables.bootstrap.min.js')}}"></script>

  <!-- custom -->
  <script src="{{asset('asset/js/main.js')}}"></script>
  <script src="{{ asset('asset/js/index.js') }}"></script>
  <script src="{{ asset('js/custom.js') }}"></script>
  <script>
    //$('#tblist').DataTable();
    var data = null;
    var status = null;
    var form = $('#formubah').serialize();
    var link = '{{url('/data/update')}}';
    $('#tblist').on('click', '.ubah', function(event) {
        event.preventDefault();
        data = $(this).closest('tr').find('td:eq(0)').text();
        var nama = $(this).closest('tr').find('td:eq(2)').text();
        var ja = $(this).closest('tr').find('td:eq(3)').text();
        var jt = $(this).closest('tr').find('td:eq(4)').text();
        $('#nama').val(nama);
        $('#ja').val(ja);
        $('#jt').val(jt);
        $('#idpengajuan').val(data);
        console.log(data);
        $('#modalubah').modal('show');
        //alert(data);
        /* Act on the event */
    });
    $('#status').change(function(event) {
      status = $(this).val();
      $('#instatus').val(status);
      console.log(status);
    });
    $('#save').click(function(event) {
    $.ajax({
      url: link,
      type: 'POST',
      data: form,
      success:function(data){
        alert('berhasil');
        location.reload();
      },
      error(value){
        console.log(value);
      }
    })


    });
    // $('.ubah').click(function(event) {
    //     event.preventDefault();
    //
    //     $('#modalubah').modal('show');
    // });
</script>
@endsection
