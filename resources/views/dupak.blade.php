@extends('layouts.template')
@section('css')
  <!-- start: Css -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/bootstrap.min.css')}}">

  <!-- plugins -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/datatables.bootstrap.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/font-awesome.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/animate.min.css')}}" />

  <link href="{{asset('asset/css/style.css')}}" rel="stylesheet">

  <link rel="shortcut icon" href="{{asset('asset/img/logomi.png')}}">

@endsection

@section('profile')
  <li class="user-name"><span>{{auth::user()->username}}</span></li>
                    <li class="dropdown avatar-dropdown">
                     <img src="{{asset('asset/img/avatar.jpg')}}" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
                     <ul class="dropdown-menu user-dropdown">
                       <li><a href="#"><span class="fa fa-user"></span> My Profile</a></li>
                       <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();"><span class="fa fa-power-off"></span> Logout</a></li>
                          {{-- Di panggil pada event onclick --}}
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>

                        </ul>
                      </li>
@endsection
@section('leftmenu')

@endsection
@section('content')
<div id="content">
<div class="tab-wrapper text-center">
  <div class="panel box-shadow-none text-left content-header">
    <div class="panel-body" style="padding-bottom:0px">
      <div class="col-md-12">
        <h3 class="animated fadeInLeft"> Form Penilaian PAK</h3>
        <p class="animated fadeInDown">
            Penilaian  <span class="fa-angle-right fa"></span>  Penilaian PAK
                </p>
      </div>



    </div>
    <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-heading"><h3>Penilaian PAK </h3>

                    </div>
                    <div class="panel-body">
                      <div class="responsive-table">
                      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th style="width:4%;">No</th>
                          <th style="width:30%;">Nama</th>
                          <th style="width:15%;">Jabatan Asal</th>
                          <th style="width:15%;">Jabatan Tujuan</th>
                          <th style="width:13%;">Link</th>
                          <th style="width:13%;">Status</th>
                          <th style="width:10%;">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                         </tbody>
                        </table>
                      </div>

                  </div>
                  <!-- Modal -->


                </div>


              </div>

              </div>
            </div>


  </div>


</div>
</div>
@endsection
@section('javascript')
  <script src="{{ asset('asset/js/jquery.min.js') }}"></script>
  <script src="{{ asset('asset/js/jquery.ui.min.js') }}"></script>
  <script src="{{ asset('asset/js/bootstrap.min.js') }}"></script>
<!-- plugins -->
  <script src="{{ asset('asset/js/plugins/moment.min.js') }}"></script>
  <script src="{{ asset('asset/js/plugins/jquery.datatables.min.js') }}"></script>
  <script src="{{ asset('asset/js/plugins/datatables.bootstrap.min.js') }}"></script>
  <script src="{{ asset('asset/js/plugins/jquery.nicescroll.js') }}"></script>
<!-- custom -->
  <script src="{{ asset('asset/js/main.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#datatables-example').DataTable();
  });
</script>
@endsection
