@extends('layouts.template')
@section('css')
  <!-- start: Css -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/bootstrap.min.css')}}">

  <!-- plugins -->
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/datatables.bootstrap.min.css')}}"/>
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/font-awesome.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/plugins/animate.min.css')}}" />
  <link href="{{asset('asset/css/style.css')}}" rel="stylesheet">
  <!-- end: Css -->

  <!-- end: Css -->
  <!-- end: Css -->

  <link rel="shortcut icon" href="{{asset('asset/img/logomi.png')}}">

@endsection

@section('profile')
  <li class="user-name"><span>{{auth::user()->username}}</span></li>
  <li class="dropdown avatar-dropdown">
      <img src="{{asset('asset/img/avatar.jpg')}}" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" />
      <ul class="dropdown-menu user-dropdown">
          <li><a href="{{route('profile')}}"><span class="fa fa-user"></span> My Profile</a></li>
          <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();"><span class="fa fa-power-off"></span> Logout</a></li>
          {{-- Di panggil pada event onclick --}}
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>

      </ul>
  </li>

@endsection
@section('leftmenu')

@endsection
@section('content')
            <!-- start: Content -->
            <div id="content">
              <div class="tab-wrapper text-center">
                <div class="panel box-shadow-none text-left content-header">
                  <div class="panel-body" style="padding-bottom:0px">
                    <div class="col-md-12">
                      <h3 class="animated fadeInLeft"> Data Penilai Eksternal</h3>
                      <p class="animated fadeInDown">
                          Penilai  <span class="fa-angle-right fa"></span>  Penilai Eksternal
                              </p>
                    </div>

                  </div>
                  <div class="col-md-12 top-20 padding-0">
                    <div class="col-md-12">
                        <div class="panel">
                          <div class="panel-heading"><h3>Penilai Eksternal </h3>
                            <button type="button" class="btn btn-round btn-danger" data-toggle="modal" data-target="#modal">
                              <span class="fa fa-plus"></span> Tambah
                            </button>
                       </div>
                    <div class="panel-body">
                      <div class="responsive-table">
                      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th style="width:4%;">No</th>
                          <th style="width:14%;">NIP</th>
                          <th style="width:11%;">NIDN</th>
                          <th style="width:16%;">Nama</th>
                          <th style="width:14%">Universitas</th>
                          <th style="width:10%">Rumpun</th>
                          <th style="width:10%">Sub Rumpun</th>
                          <th style="width:10%">Jabatan Fungsional</th>
                          <th style="width:10%;">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>

                         @php $no = 1; @endphp
                         @foreach ( $eksternal as $data)
                              <tr id="{{$data->id}}">
                                <td style="text-align: center;">{{ $no++ }}</td>
                                <td>{{ $data->nip }}</td>
                                <td>{{ $data->nidn }}</td>
                                <td>{{ $data->nama }}</td>
                                <td>{{ $data->universitas }}</td>
                                <td>{{ $data->rumpun }}</td>
                                <td>{{ $data->sub_rumpun }}</td>
                                <td>{{ $data->jabatan }}</td>
                                <td>
                                    <button id="update" type="button" class="btn-warning btn btn-circle btn-mn edit" style="submit" data-toggle="modal" data-target="#modal2" value="{{ $data->id }}" data-id="{{ $data->id }}"><span class="fa fa-pencil"></span>
                                    </button>
                                      <button type="submit" class="btn btn-circle btn-mn btn-danger" data-toggle="modal" data-target="#modal3" value="{{ $data->id }}" data-id="{{ $data->id }}"><span class="fa fa-trash"></span>
                                    </button>
                                </td>
                          </tr>
                          @endforeach
                        </tbody>

                        </table>

                      </div>
                      <div id="modal3" class="modal animated fadeInUp" >
                                  <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                       <form action="{{ route('penilaieksternal.destroy', $data->id) }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Hapus?</h4>
                                      </div>
                                      <div class="modal-body" >
                                        <p>Hapus Data Penilai {{ $data->nama }}?</p>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                      </div>
                                    </form>
                                    </div><!-- /.modal-content -->
                                  </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->

                        <div id="modal2" class="modal animated fadeInUp" >
                                  <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                      <form action="{{ route('penilaieksternal.update', $data->id) }}" method="post">
                                      {{ csrf_field() }}
                                      {{ method_field('PUT') }}

                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Edit Data Penilai {{ $data->nama }}</h4>
                                      </div>
                                      <div class="modal-body">
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-3 control-label text-right" style="padding-top: 6px; font-size: 15px;">Nama</label>
                                         <div class="col-sm-8"><input type="text" class="form-control nama" id="nama" name="nama" value="{{ $data->nama }}"></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-3 control-label text-right" style="padding-top: 6px; font-size: 15px;">NIP</label>
                                         <div class="col-sm-8"><input type="text" class="form-control nip" id="nip" name="nip" value="{{ $data->nip }}"></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-3 control-label text-right" style="padding-top: 6px; font-size: 15px;">NIDN</label>
                                         <div class="col-sm-8"><input type="text" class="form-control nidn" id="nidn" name="nidn" value="{{ $data->nidn }}"></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-3 control-label text-right" style="padding-top: 6px; font-size: 15px;">Password</label>
                                         <div class="col-sm-8"><input type="password" class="form-control password" id="password" name="password" value="{{ $data->password }}"></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-3 control-label text-right" style="padding-top: 6px; font-size: 15px;">Universitas</label>
                                          <div class="col-sm-8">
                                            <select class="form-control universitas" id="universitas" name="universitas">
                                              @foreach($universitas as $univ)
                                                @if ($univ->nama_univ == $data->universitas)
                                                <option {{ $univ->nama_univ == $data->universitas ? 'selected':'' }}> {{$data->universitas}} </option>
                                                @else
                                                <option value="{{ $univ->nama_univ }}">{{ $univ->nama_univ}}</option>
                                                @endif
                                              @endforeach
                                            </select>
                                          </div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px">
                                          <label class="col-sm-3 control-label text-right" style="padding-top: 6px; font-size: 15px;">Rumpun</label>
                                          <div class="col-sm-8">
                                            <select class="form-control rumpun" id="rumpun" name="rumpun">
                                              @foreach($rumpun as $rump)
                                                @if ($rump->nama_rumpun == $data->rumpun)
                                                <option {{ $rump->nama_rumpun == $data->rumpun ? 'selected':'' }}> {{$data->rumpun}} </option>
                                                @else
                                                <option value="{{ $rump->nama_rumpun }}">{{ $rump->nama_rumpun}}</option>
                                                @endif
                                              @endforeach
                                            </select>
                                          </div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-3 control-label text-right" style="padding-top: 6px; font-size: 15px;">Sub Rumpun</label>
                                         <div class="col-sm-8">
                                            <select class="form-control sub_rumpun" id="sub_rumpun" name="sub_rumpun">
                                              @foreach($sub_rumpun as $srump)
                                                @if ($srump->nama_rumpun_sub == $data->sub_rumpun)
                                                <option {{ $srump->nama_rumpun_sub == $data->sub_rumpun ? 'selected':'' }}> {{$data->sub_rumpun}} </option>
                                                @else
                                                <option value="{{ $srump->nama_rumpun_sub }}">{{ $srump->nama_rumpun_sub}}</option>
                                                @endif
                                              @endforeach
                                            </select>
                                          </div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-3 control-label text-right" style="padding-top: 6px; font-size: 15px;">Jabatan Fungsional</label>
                                          <div class="col-sm-8">
                                            <select class="form-control jabatan" id="jabatan" name="jabatan">
                                               @foreach($jabatan as $jab)
                                                  @if ($jab->nama_jafung == $data->jabatan)
                                                  <option {{ $jab->nama_jafung == $data->jabatan ? 'selected':'' }}> {{$data->jabatan}} </option>
                                                  @else
                                                  <option value="{{ $jab->nama_jafung }}">{{ $jab->nama_jafung }}</option>
                                                  @endif
                                               @endforeach
                                            </select>
                                          </div>
                                        </div>


                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-primary">Ubah</button>
                                      </div>
                                    </form>

                                    </div><!-- /.modal-content -->
                                  </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                  </div>

                  <!-- Modal -->





                  </div>
                  <div id="modal" class="modal animated fadeInUp" >
                                  <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                      <form action="{{ route('penilaieksternal.store') }}" method="post">

                                        @csrf
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Tambah Penilai Eksternal</h4>
                                      </div>
                                      <div class="modal-body">
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-3 control-label text-right" style="padding-top: 6px; font-size: 15px;">Nama</label>
                                         <div class="col-sm-8"><input type="text" class="form-control" id="nama" name="nama" required></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-3 control-label text-right" style="padding-top: 6px; font-size: 15px;">NIP</label>
                                         <div class="col-sm-8"><input type="text" class="form-control" id="nip" name="nip" required></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-3 control-label text-right" style="padding-top: 6px; font-size: 15px;">NIDN</label>
                                         <div class="col-sm-8"><input type="text" class="form-control" id="nidn" name="nidn" required></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-3 control-label text-right" style="padding-top: 6px; font-size: 15px;">Password</label>
                                         <div class="col-sm-8"><input type="password" class="form-control" id="password" name="password" required></div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-3 control-label text-right" style="padding-top: 6px; font-size: 15px;">Universitas</label>
                                          <div class="col-sm-8">
                                            <select class="form-control" id="universitas" name="universitas" required>
                                              @foreach($universitas as $univ)
                                              <option value="{{ $univ->nama_univ }}">{{ $univ->nama_univ}}</option>
                                              @endforeach
                                            </select>
                                          </div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px">
                                          <label class="col-sm-3 control-label text-right">Rumpun</label>
                                          <div class="col-sm-8">
                                            <select class="form-control" id="rumpun" name="rumpun" required>
                                              @foreach($rumpun as $rump)
                                              <option value="{{ $rump->nama_rumpun}}">{{ $rump->nama_rumpun}}</option>
                                              @endforeach
                                            </select>
                                          </div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-3 control-label text-right" style="padding-top: 6px; font-size: 15px;">Sub Rumpun</label>
                                         <div class="col-sm-8">
                                            <select class="form-control" id="sub_rumpun" name="sub_rumpun" required>
                                              @foreach($sub_rumpun as $srump)
                                              <option value="{{ $srump->nama_rumpun_sub}}">{{ $srump->nama_rumpun_sub}}</option>
                                              @endforeach
                                            </select>
                                          </div>
                                        </div>
                                        <div class="form-group" style="padding-bottom:50px"><label class="col-sm-3 control-label text-right" style="padding-top: 6px; font-size: 15px;">Jabatan Fungsional</label>
                                          <div class="col-sm-8">
                                            <select class="form-control" id="jabatan" name="jabatan" required>
                                              @foreach($jabatan as $jab)
                                              <option value="{{ $jab->nama_jafung}}">{{ $jab->nama_jafung}}</option>
                                              @endforeach
                                            </select>

                                          </div>
                                        </div>


                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                      </div>

                                    </form>
                                    </div><!-- /.modal-content -->
                                  </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->

                </div>
              </div>

              </div>
            </div>
          </div>
                        <!-- end: content -->
@endsection
@section('javascript')
  <!-- start: Javascript -->
  <script src="{{asset('asset/js/jquery.min.js')}}"></script>
  <script src="{{asset('asset/js/jquery.ui.min.js')}}"></script>
  <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>


  <!-- plugins -->
  <script src="{{asset('asset/js/plugins/moment.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.knob.js')}}"></script>
  <script src="{{asset('asset/js/plugins/ion.rangeSlider.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/bootstrap-material-datetimepicker.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.nicescroll.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.mask.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/select2.full.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/nouislider.min.js')}}"></script>
  <script src="{{asset('asset/js/plugins/jquery.validate.min.js')}}"></script>
  <!-- custom -->
  <script src="{{asset('asset/js/main.js')}}"></script>
  <!-- end: Javascript -->
  <!-- custom -->
   <script src="{{asset('asset/datejs/datepicker.en-US.js')}}"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
  <script src="https://fengyuanchen.github.io/js/common.js"></script>
  <script src="{{asset('asset/date/js/main.js')}}"></script>
  <script src="{{asset('asset/date/js/datepicker.js')}}"></script>
  <!-- end: Javascript -->
<script type="text/javascript">
  $(document).ready(function(){
    $('#datatables-example').DataTable();
  });
</script>
<!-- end: Javascript -->
@endsection
