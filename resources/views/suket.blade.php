<html>
	<head>
	<title>Sistem Informasi Kenaikan Pangkat</title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link rel="icon" type="image/x-icon" href="images/favicon.png">
	<style>
		.tab_header { border-bottom: 1px solid black; margin-bottom: 5px }
		.div_headeritem { float: left }
		.div_preheader, .div_header { font-family: "Times New Roman" }
		.div_preheader { font-size: 15px; font-weight: bold }
		.div_header { font-size: 12px; font-weight: bold; }
		.div_headertext { font-size: 12px; font-style: italic }

		.tb_head td, .div_head, .div_subhead { font-family: "Times New Roman" }
		.tb_head { border-bottom: 1px solid black }
		.tb_head td { font-size: 14px }
		.tb_head .mark { font-size: 11px }
		.div_head { font-size: 21px; text-decoration: underline }
		.div_subhead { font-size: 14px; margin-bottom: 5px }
		.div_head, .div_subhead { font-weight: bold }

		.tb_data { border: 1px solid black; border-collapse: collapse }
		.tb_data th, .tb_data td { border: 1px solid black; font-family: "Times New Roman"; padding: 5px }
		.tb_data th { background-color: #CFC; font-size: 14px }
		.tb_data td { font-size: 14px }
		.tb_data .noborder th { border-left: none; border-right: none }

		.tb_subfoot, .tb_foot { font-family: "Times New Roman" }
		.tb_subfoot { font-size: 14px; border-top: 1px solid black }
		.tb_foot { font-size: 14px; font-weight: bold; margin-top: 10px; }
		.tb_foot .mark { font-size: 14px; font-weight: normal }
		.tb_foot .pad { padding-left: 30px }
		.tb_awal { font-size: 14px; font-weight: bold;  border-top: 1px solid black; border-left: 1px solid black; border-right: 1px solid black }
		.tb_awal .mark { font-size: 14px; font-weight: normal }
		.tb_awal .pad { padding-left: 30px }
	</style>
</head>
<body>
<div align="center">
<br>
<table class="tab_header" width="780">
<thead>
	<tr>
		<td width="70" align="center">
			<img src="baru-2.jpg" width="90">
		</td>
		<td valign="top" colspan="5"  align="center">
			<div class="div_preheader">KEMENTERIAN AGAMA</div>
			<div class="div_header">UNIVERSITAS ISLAM NEGERI SUNAN AMPEL - SURABAYA</div>
						<div class="div_header">FAKULTAS SAINS DAN TEKNOLOGI</div>
						<div class="div_headertext">Jl. Ahmad Yani 117 Surabaya. Telp. 031-8410298 Fax. 031-8413300. website: www.sunan-ampel.ac.id, email:info@sunan-ampel.ac.id</div>
		</td>
	</tr>
	</thead>
</table>
<table class="tb_foot" width="780">
	<tbody>
	<tr>
		<td width="500">&nbsp;</td>
		<td class="mark">Kepada Yth</td>
	</tr>
	<tr>
		<td width="500">&nbsp;</td>
		<td class="mark">Rektor UIN Sunan Ampel</td>
	</tr>
	<tr>
		<td width="500">&nbsp;</td>
		<td class="mark">di</td>
	</tr>
</tbody>
</table>
<table class="tb_foot" width="780" style="margin-bottom: 30px">
	<tr>
		<td width="540">&nbsp;</td>
		<td class="mark">Surabaya</td>
	</tr>
</table>
<div class="div_head" style="margin-bottom: 10px">Daftar Usul Mutasi</div>
<div class="div_subhead" style="margin-bottom: 40px">Nomor : </div>
@foreach ($fakultas as $data )


<table class="tb_awal" width="780">
<tr>
	<td class="mark" width="200">Nama<td>
	<td class="mark" width="190">{{$data->nama_pegawai}}</td>
	<td class="mark" width="200">NIP<td>
	<td class="mark" width="190">{{$data->nip}}</td>
</tr>
<tr>
	<td class="mark" width="200">Tempat dan Tanggal Lahir<td>
	<td class="mark" width="190">Surabaya,10-Juli-1998</td>
	<td class="mark" width="200">Gol./Ruang<td>
	<td class="mark" width="190">SIKP31838</td>
</tr>
<tr>
	<td class="mark" width="200">Pangkat<td>
	<td class="mark" width="190">Sek Prodi</td>
	<td class="mark" width="200">Nomor<td>
	<td class="mark" width="190">01287</td>
</tr>
<tr>
	<td class="mark" width="200">Jabatan<td>
	<td class="mark" width="190">{{$data->jafung_sekarang}}</td>
	<td class="mark" width="200"><td>
	<td class="mark" width="190"></td>
</tr>
<tr>
	<td class="mark" width="200">Bertanggung jawab kepada<td>
	<td class="mark" width="190">Fakultas</td>
	<td class="mark" width="200"><td>
	<td class="mark" width="190"></td>
</tr>
<tr>
	<td class="mark" width="200">Atasan Langsung<td>
	<td class="mark" width="190"></td>
	<td class="mark" width="200"><td>
	<td class="mark" width="190"></td>
</tr>
<tr>
	<td class="mark" width="200"><td>
	<td class="mark" width="190"></td>
	<td class="mark" width="200"><td>
	<td class="mark" width="190"></td>
</tr>
<tr>
	<td class="mark" width="200"><td>
	<td class="mark" width="190"></td>
	<td class="mark" width="200"><td>
	<td class="mark" width="190"></td>
</tr>
<tr>
	<td class="mark" width="200"><td>
	<td class="mark" width="190"></td>
	<td class="mark" width="200"><td>
	<td class="mark" width="190"></td>
</tr>
<tr>
	<td class="mark" width="200"><td>
	<td class="mark" width="190"></td>
	<td class="mark" width="200">Nama<td>
	<td class="mark" width="190">Dr.H. Ah Ali Arifin, MM</td>
</tr>
<tr>
	<td class="mark" width="200"><td>
	<td class="mark" width="190"></td>
	<td class="mark" width="200">Pangkat Rill<td>
	<td class="mark" width="190">IV/a</td>
</tr>
</table>
@endforeach
<table class="tb_awal" width="780">
	<tr>
		<td class="mark" width="300">Usul Mutasi<td>
		<td class="mark" width="300">Kenaikan Pangkat Dari<td>
	</tr>
	<tr>
		<td class="mark" width="300">Alasan/Rekomendasi<td>
		<td class="mark" width="300">1. Formasi Mengijinkan<td>
	</tr>
</table>
<table class="tb_awal" width="780">
	<tr>
		<td class="mark" width="300">2.Nomor<td>
		<td class="mark" width="300" align="center" style="border-left: 1px solid black">Surabaya, 31 Desember 2018<td>
	</tr>
	<tr>
		<td class="mark" width="300" align="center">Menyetujui:<td>
		<td class="mark" width="300" align="center" style="border-left: 1px solid black">Pengusul<td>
	</tr>
	<tr>
		<td class="mark" width="300"><td>
		<td class="mark" width="300" align="center" style="border-left: 1px solid black; margin-bottom: 50px">Dekan<td>
	</tr>
	<tr>
		<td class="mark" width="300"><td>
		<td class="mark" width="300" align="center" style="border-left: 1px solid black">Dr.H. Ah Ali Arifin, MM<td>
	</tr>
	<tr>
		<td class="mark" width="300"><td>
		<td class="mark" width="300" align="center" style="border-left: 1px solid black">NIP. 1962121419930331002<td>
	</tr>
</table>
</body>
</html>
