<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// model penilai eksternal
class Penilai extends Model
{
    protected $table = 'penilai_eksternal';
    public $timestamps = false;
}
