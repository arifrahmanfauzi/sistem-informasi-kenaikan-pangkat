<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AngkaKreditController extends Controller
{
    public function index(){
      $data = DB::table('view_rubrik')->get();
      return view('angkakredit',['data' => $data]);
    }


    public function postpak(Request $request){
      $nilai = 0;
      DB::table('rubrik')->insert([
        'id_sub_bidang'=>$request->idbidang,
        'id_dupak'=>$request->iddupak,
        'isian_rubrik'=>$request->uraian,
        'id_satuan_hasil'=>$request->satuan,
        'angka_kredit_total'=>$request->angkakredit,
        'angka_kredit_penilai' => $request->iddupak,
        'berkas'=>$request->link,
        'tanggal_mulai'=>$request->datestart,
        'tanggal_berakhir'=>$request->dateend,
      ]);
      // $data = $request->all();
      // dd($data);
      //return redirect('/pak');
      return back()->withInput();
    }
}
