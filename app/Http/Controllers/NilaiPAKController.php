<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class NilaiPAKController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // menampilkan data pengajuan yang akan dinilai
    public function index()
    {
        $penilaian = DB::table('view_pengajuan')->where('status','sedang dinilai')->orWhere('status', 'menunggu dinilai')->get();
        return view('penilai.pilih-pak',['penilaian' => $penilaian]);
    }

    // menampilkan data dupak dari pengajuan yang akan dinilai
    public function getunsur2($id){
      $getpengajuan = DB::table('view_pengajuan')->where('id','=',$id)->get(); 
      $getrubrik = DB::table('view_rubrik')->where('id_dupak','=',$id)->get();
      $angka_total = DB::table('view_rubrik')->where('id_dupak','=',$id)->sum('angka_kredit_total');
      $total_penilai = DB::table('view_rubrik')->where('id_dupak','=',$id)->sum('angka_kredit_penilai');
      $ak_pend = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Pendidikan')->sum('angka_kredit_total');
      $pen_pend = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Pendidikan')->sum('angka_kredit_penilai');
      $ak_pen = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Penunjang')->sum('angka_kredit_total');
      $pen_pen = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Penunjang')->sum('angka_kredit_penilai');
      $ak_pel = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Pelaksana Pendidikan')->sum('angka_kredit_total');
      $pen_pel = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Pelaksana Pendidikan')->sum('angka_kredit_penilai');
      $ak_pene = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Penelitian')->sum('angka_kredit_total');
      $pen_pene = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Penelitian')->sum('angka_kredit_penilai');
      $ak_peng = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Pengabdian')->sum('angka_kredit_total');
      $pen_peng = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Pengabdian')->sum('angka_kredit_penilai');
      return view('penilai.nilai-pak',['getrubrik'=>$getrubrik, 'getpengajuan'=>$getpengajuan, 'angka_total' => $angka_total, 'total_penilai' => $total_penilai, 'ak_pend' => $ak_pend, 'pen_pend' => $pen_pend, 'ak_pen' => $ak_pen, 'pen_pen' => $pen_pen, 'ak_pel' => $ak_pel, 'pen_pel' => $pen_pel, 'ak_pene' => $ak_pene, 'pen_pene' => $pen_pene, 'ak_peng' => $ak_peng, 'pen_peng' => $pen_peng]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    // mendapatkan id rubrik
    public function dapat_rubrik($id){
        $getrubrik = DB::table('view_rubrik')->where('id_rubrik','=',$id)->get();
        return view('penilai.isi-nilai',['getrubrik' => $getrubrik]);
    }

    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('rubrik')
        ->where('id_rubrik', $id)
        ->update(['angka_kredit_penilai' => $request->kredit_penilai]);
        $id_dupak = $request->id_dupak;
        $url = '/nilai-pak/'.$id_dupak;
        return redirect()->to($url)->with('alert-success','Berhasil Menilai!');
          //return redirect()->url('nilai-pak.id')->with('alert-success','Berhasil Menilai!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

     
    }
}
