
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Html\FormFacade;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Penilai;

class PenilaiEksternalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // memampilkan tabel penilai eksternal
    public function index()
    {
        $eksternal = DB::table('penilai_eksternal')->get();
        return view('kepegawaian.penilai-eksternal',['eksternal' => $eksternal]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    // halaman tambah data penilai eksternal
    public function tambah_data(){
        $universitas= DB::table('m_universitas')->select('nama_univ')->get();
        $rumpun = DB::table('m_rumpun')->select('nama_rumpun')->get();
        $sub_rumpun = DB::table('m_rumpun_sub')->select('nama_rumpun_sub')->get();
        $jabatan = DB::table('m_jafung')->select('nama_jafung')->paginate(4);
        return view('kepegawaian.tambah-penilai',['universitas' => $universitas, 'rumpun' => $rumpun,
            'sub_rumpun' => $sub_rumpun, 'jabatan' => $jabatan]);
    }

    // menambahkan data penilai eksternal
    public function store(Request $request)
    {
        DB::table('penilai_eksternal')->insert([
        ['nama' => $request->nama,
         'nip' => $request->nip,
         'nidn' => $request->nidn,
         'password' => Hash::make($request->password),
         'universitas' => $request->universitas,
         'rumpun' => $request->rumpun,
         'sub_rumpun' => $request->sub_rumpun,
         'jabatan' => $request->jabatan]
      ]);

      return redirect()->route('penilai-eksternal.index')->with('alert-success','Berhasil Menambahkan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // mendapat id
    public function edit_data($id)
    {
        $universitas= DB::table('m_universitas')->select('nama_univ')->get();
        $rumpun = DB::table('m_rumpun')->select('nama_rumpun')->get();
        $sub_rumpun = DB::table('m_rumpun_sub')->select('nama_rumpun_sub')->get();
        $jabatan = DB::table('m_jafung')->select('nama_jafung')->paginate(4);
        $eksternal = DB::table('penilai_eksternal')->where('id_penilai_eks','=',$id)->get();
        return view('kepegawaian.edit-penilai',['eksternal'=>$eksternal, 'universitas' => $universitas, 'rumpun' => $rumpun,
            'sub_rumpun' => $sub_rumpun, 'jabatan' => $jabatan]);
    }

    // update data penilai eksternal
    public function update(Request $request, $id)
    {
        DB::table('penilai_eksternal')
        ->where('id_penilai_eks', $id)
        ->update(['nama' => $request->nama,
         'nip' => $request->nip,
         'nidn' => $request->nidn,
         'password' => Hash::make($request->password),
         'universitas' => $request->universitas,
         'rumpun' => $request->rumpun,
         'sub_rumpun' => $request->sub_rumpun,
         'jabatan' => $request->jabatan]);
          return redirect()->route('penilai-eksternal.index')->with('alert-success','Berhasil Mengubah Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // hapus data penilai eksternal
    public function destroy($id)
    {
        DB::table('penilai_eksternal')->where('id_penilai_eks',$id)->delete();
        return redirect()->route('penilai-eksternal.index')->with('alert-success','Berhasil Menghapus Data!');
    }
}
