<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      // if (Auth::user()->is_permissions == 0) {
      //   return view('user');
      // }
      if (Auth::user()->is_permissions == 1) {
        return view('admin');

      }elseif (Auth::user()->is_permissions == 2) {
      return redirect('/data');
      }
        return view('home2');
    }


    public function allUsers()

{

    dd('Access All Users');

}
  public function logout(){
    Auth::logout();
    return Redirect::route('/');
}


/**

 * Show the application dashboard.

 *

 * @return \Illuminate\Http\Response

 */

public function adminSuperadmin()

{

    dd('Access Admin and Superadmin');

}


/**

 * Show the application dashboard.

 *

 * @return \Illuminate\Http\Response

 */

public function superadmin()

{

    dd('Access only Superadmin');

}
public function users(){
$users = User::all();

  return view('kelolausers',['user'=>$users]);
}
public function dupak(){

  return view('dupak');
}
public function getunsur($id){
  $getrubrik = DB::table('view_rubrik')->where('id_dupak','=',$id)->get();
  $getunsur = DB::table('unsur')->get();
  $getnilai = DB::table('view_nilai_unsur')->get();
  //
  $getpengajuan = DB::table('view_pengajuan')->where('id','=',$id)->get();
  //$getrubrik = DB::table('view_rubrik')->where('id_dupak','=',$id)->get();
  $angka_total = DB::table('view_rubrik')->where('id_dupak','=',$id)->sum('angka_kredit_total');
  $total_penilai = DB::table('view_rubrik')->where('id_dupak','=',$id)->sum('angka_kredit_penilai');
  $ak_pend = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Pendidikan')->sum('angka_kredit_total');
  $pen_pend = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Pendidikan')->sum('angka_kredit_penilai');
  $ak_pen = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Penunjang')->sum('angka_kredit_total');
  $pen_pen = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Penunjang')->sum('angka_kredit_penilai');
  $ak_pel = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Pelaksana Pendidikan')->sum('angka_kredit_total');
  $pen_pel = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Pelaksana Pendidikan')->sum('angka_kredit_penilai');
  $ak_pene = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Penelitian')->sum('angka_kredit_total');
  $pen_pene = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Penelitian')->sum('angka_kredit_penilai');
  $ak_peng = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Pengabdian')->sum('angka_kredit_total');
  $pen_peng = DB::table('view_rubrik')->where('id_dupak','=',$id)->where('nama_unsur','=','Pengabdian')->sum('angka_kredit_penilai');
  return view('inputpak',['getnilai'=>$getnilai,'getrubrik'=>$getrubrik, 'getpengajuan'=>$getpengajuan, 'angka_total' => $angka_total, 'total_penilai' => $total_penilai, 'ak_pend' => $ak_pend, 'pen_pend' => $pen_pend, 'ak_pen' => $ak_pen, 'pen_pen' => $pen_pen, 'ak_pel' => $ak_pel, 'pen_pel' => $pen_pel, 'ak_pene' => $ak_pene, 'pen_pene' => $pen_pene, 'ak_peng' => $ak_peng, 'pen_peng' => $pen_peng, 'id'=>$id]);
}

public function getsubunsur(Request $request){
  $getsubunsur = DB::table('sub_unsur')->where('id_unsur', $request->id_unsur)->pluck('id_unsur','nama_sub_unsur');

  return response()->json($getsubunsur);
}
public function getsubbidang(Request $request){
  $getsubunsur = DB::table('subbidang')->where('id_sub_unsur', $request->id_sub_unsur)->pluck('id_sub_bidang','nama_sub_bidang');

  return response()->json($getsubunsur);
}
public function getangkakredit(Request $request){
  $getangkakredit = DB::table('subbidang')->where('id_sub_bidang',$request->id_sub_bidang)->first();

  return response()->json($getangkakredit);
}
public function profile(){
  $n = Auth::user()->id_pegawai;
  $nama = DB::table('tbpegawai')->where('id', $n)->get();
  //$nama = DB::table('tbpegawai')->select('nama')->where('id', $n)->value('nama');
  // pakai ->value('nama')
  //bisa pakai ->pluck('nama')
  //dd( $nama);
  $data = array('nama' => $nama);
  //return view('profile',$data);
  return view('profile',['nama'=>$nama]);
}
}
